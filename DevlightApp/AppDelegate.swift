//
//  AppDelegate.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 15.10.2020.
//

import UIKit
import AlamofireNetworkActivityLogger

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let firstVC = LaunchViewController()
        window?.rootViewController = UINavigationController(rootViewController: firstVC)
        window?.makeKeyAndVisible()
        
        NetworkActivityLogger.shared.level = .debug
        NetworkActivityLogger.shared.startLogging()
                   
        return true
    }
}
