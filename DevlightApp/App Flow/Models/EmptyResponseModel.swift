//
//  EmptyResponseModel.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 08.12.2020.
//

import Foundation

struct EmptyResponseModel: Decodable {}
