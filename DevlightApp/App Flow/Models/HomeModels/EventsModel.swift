//
//  EventsModel.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 04.02.2021.
//

import Foundation

struct RequestEventsModel: Codable {
    let page: Int?
    let limit: Int?
}

struct RequestEvensIdModel: Codable {
    let eventId: String
    
    enum CodingKeys: String, CodingKey {
        case eventId = "id"
    }
}

struct ResponseEventByIdModel: Codable {
    let name: String?
    let description: String?
    let date: Int?
    let image: String?
    let eventId: String?
    let link: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case description = "description"
        case date = "date"
        case image = "image"
        case eventId = "id"
        case link = "link"
    }

}

struct ResponseEventsModel: Codable {
    let name: String?
    let description: String?
    let date: Int?
    let image: String?
    let eventId: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case description = "description"
        case date = "date"
        case image = "image"
        case eventId = "id"
    }
}

struct EmptyParametersModel: Encodable {
}
