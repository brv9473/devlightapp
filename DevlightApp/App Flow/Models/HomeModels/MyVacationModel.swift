//
//  MyVacationModel.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 04.02.2021.
//

import Foundation

struct VacationObject: Decodable {
    let available: Int?
    let used: Int?
}

struct MyVacation: Decodable {
    let vacation: VacationObject?
    let sickLeave: VacationObject?
    let remotes: VacationObject
}
