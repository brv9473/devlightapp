//
//  ProjectsModels.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 22.02.2021.
//

import Foundation

struct RequestProjectsModel: Codable {
    let page: Int?
    let limit: Int?
}

struct ResponseProjectModel: Codable {
    let projectId: String
    let icon: String
    let name: String
    let description: String
    
    enum CodingKeys: String, CodingKey {
        case projectId = "id"
        case icon = "icon"
        case name = "name"
        case description = "description"
    }
}

struct ProjectDetalizationModel: Codable {
    let workers: [String]?
    let icon: String?
    let status: String?
    let idProject: String?
    let dateOfStart: Int?
    let description: String?
    let name: String?
    let platforms: PlatformsModel?
    
    enum CodingKeys: String, CodingKey {
        case workers = "workers"
        case icon = "icon"
        case status = "status"
        case idProject = "id"
        case description = "description"
        case name = "name"
        case platforms = "platforms"
        case dateOfStart = "dateOfStart"
    }
}

struct PlatformsModel: Codable {
    let android: AndroidModel?
    let ios: IosModel?
}

struct AndroidModel: Codable {
    let link: String?
}

struct IosModel: Codable {
    let link: String?
}
