//
//  ProfileModel.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 23.02.2021.
//

import Foundation

struct ProfileResponseModel: Decodable {
    let phoneNumber: String?
    let lastName: String?
    let position: String?
    let avatar: String?
    let profileId: String?
    let dateOfBirth: Int?
    let name: String?
    let teamLead: Bool?
    
    enum CodingKeys: String, CodingKey {
        case phoneNumber = "phoneNumber"
        case lastName = "lastName"
        case position = "position"
        case avatar = "avatar"
        case profileId = "id"
        case dateOfBirth = "dateOfBirth"
        case name = "name"
        case teamLead = "teamLead"
    }
}
