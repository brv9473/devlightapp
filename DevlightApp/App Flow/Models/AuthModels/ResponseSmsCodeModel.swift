//
//  ResponseSmsCodeModel.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 09.11.2020.
//

import Foundation

struct ResponseSmsCodeModel: Decodable {
    let token: String
    let refreshToken: String
}

struct RefreshTokenModel: Codable {
    let refreshToken: String
}
