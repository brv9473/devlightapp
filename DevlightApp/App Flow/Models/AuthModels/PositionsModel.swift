//
//  PositionsModel.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 22.10.2020.
//

import Foundation

struct PositionsModel: Codable {
    let positionId: String
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case positionId = "id"
        case name = "name" 
    }
}
