//
//  FillInfoModel.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 24.11.2020.
//

import Foundation

struct FillInfoModel: Encodable {
    let name: String
    let lastName: String
    let position: String
    let teamLead: Bool?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case lastName = "lastName"
        case position = "position"
        case teamLead = "team_lead"
    }
}

struct FillInfoResponseModel: Codable {
    let name: String?
    let lastName: String?
    let position: String?
    let teamLead: Bool?
    let phoneNumber: String?
    let card: Int?
    let userId: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case lastName = "lastName"
        case position = "position"
        case teamLead = "team_lead"
        case phoneNumber = "phoneNumber"
        case card = "card"
        case userId = "id"
    }
}
