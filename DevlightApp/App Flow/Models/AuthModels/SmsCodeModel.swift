//
//  SmsCodeModel.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 23.10.2020.
//

import Foundation

struct SmsCodeModel: Encodable {
    let phoneNumber: String
    let code: Int
}
