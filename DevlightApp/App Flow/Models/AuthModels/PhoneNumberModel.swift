//
//  PhoneNumberModel.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 23.10.2020.
//

import Foundation

struct PhoneNumberModel: Codable {
    let phoneNumber: String
}
