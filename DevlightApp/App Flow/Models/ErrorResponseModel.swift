//
//  ErrorResponseModel.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 08.12.2020.
//

import Foundation

struct ErrorResponseModel: Decodable, Error {
    let error: String
    let code: Int
    
    var localizedDescription: String {
        error
    }
}
