//
//  HomeViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 27.01.2021.
//

import UIKit

class HomeViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak private var scrollView: UIScrollView!
    @IBOutlet weak private var contentView: UIView!
    @IBOutlet weak private var userInfoView: UserInfoView!
    @IBOutlet weak private var newInfoView: NewInfoView!
    @IBOutlet weak private var userCardView: UserCardView!
    @IBOutlet weak private var backImportantEventsView: UIView!
    @IBOutlet weak private var importantEventsHeader: ImportantEventsHeaderView!
    @IBOutlet weak private var importantEventsCollectionView: UICollectionView!
    @IBOutlet weak private var weekendView: WeekendView!
    @IBOutlet weak private var careerView: CareerView!
    
    // MARK: - Properties
    
    private let service = Service()
    private var vacations: MyVacation?
    private var events: [ResponseEventsModel]?
    private var positions: [PositionsModel]?
    private let dispGroup = DispatchGroup()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userInfoView.delegate = self
        importantEventsCollectionView.delegate = self
        importantEventsCollectionView.dataSource = self
        importantEventsCollectionView.register(UINib(nibName: "ImportantEventsCollectionViewCell", bundle: nil),
                                               forCellWithReuseIdentifier: "importantEventsCollectionViewCell")
        newInfoView.addActionForButtonClose(target: self, action: #selector(tapClose), event: .touchUpInside)
        let tap = UITapGestureRecognizer(target: self, action: #selector(onEventsHeaderTap))
        importantEventsHeader.addGestureRecognizer(tap)
        
        showEvents()
        showMyVacation()
        getUserInfo()
        setupUI()
        
//        dispGroup.notify(queue: .main) {
//            self.importantEventsCollectionView.reloadData()
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)

    }

    // MARK: - Private Methods
    
    private func setupUI() {
        self.view.backgroundColor = .background
        contentView.backgroundColor = .background
        
        backImportantEventsView.layer.cornerRadius = 16
        backImportantEventsView.layer.masksToBounds = true
    }
    
    private func getUserInfo() {
        let defaults = UserDefaults.standard
        
        if let decodedData = defaults.object(forKey: "userModel") as? Data {
            if let userModel = try? JSONDecoder().decode(FillInfoResponseModel.self, from: decodedData) {
                
                self.service.getPositions { [weak self] userPositions in
                    switch userPositions {
                    case .success(let model):
                        self?.positions = model
                        
                        if let pos = self?.positions?.filter({ (position) -> Bool in
                            position.positionId == userModel.position
                        }).first {
                            self?.userInfoView.addUserInfo(nameAndLastName:
                                                            "\(userModel.name ?? "???")" +
                                                            " " +
                                                            "\(userModel.lastName ?? "????")")
                            self?.userCardView.addCardNumber(number: "\(userModel.card ?? 0)")
                            
                            self?.userInfoView.addUserPosition(position: "\(pos.name)")
                            self?.careerView.addUserPosition(position: "\(pos.name)")
                        }
                        
                    case .failure(let error):
                        self?.showAlert(with: error)
                    }
                }
            }
        }
    }
    
    private func showMyVacation() {
//        dispGroup.enter()
        
        self.service.getMyVacation { [weak self] vacations in
            switch vacations {
            case .success(let model):
                self?.vacations = model
                
                DispatchQueue.main.async {
                    self?.weekendView.addVacationDays(days: "\(model.vacation?.available ?? 00)")
                    self?.weekendView.addVacationUsedDays(days: "\(model.vacation?.used ?? 00)")
                    self?.weekendView.addRemotesDays(days: "\(model.remotes.available ?? 00)")
                    self?.weekendView.addRemotesUsedDays(days: "\(model.remotes.used ?? 00)")
                    self?.weekendView.addSickLeaveDays(days: "\(model.sickLeave?.available ?? 00)")
                    self?.weekendView.addSickLeaveUsedDays(days: "\(model.sickLeave?.used ?? 00)")
                }
                
            case .failure(let error):
                self?.showAlert(with: error)
            }
//            self?.dispGroup.leave()
//            _ = self?.dispGroup.wait(timeout: DispatchTime.now() + 2)
        }
    }
    
    private func showEvents() {
//        dispGroup.enter()
        
        self.service.getEvents(parameters: RequestEventsModel(page: 0,
                                                              limit: 10)) { [weak self] events in
            switch events {
            case .success(let model):
                self?.events = model
                self?.importantEventsCollectionView.reloadData()
                
            case .failure(let error):
                self?.showAlert(with: error)
            }
//            self?.dispGroup.leave()
//            _ = self?.dispGroup.wait(timeout: DispatchTime.now() + 2)
        }
    }
    
    @objc private func tapClose() {
        UIView.animate(withDuration: 0.6) {
            self.newInfoView.alpha = 0
            self.newInfoView.translatesAutoresizingMaskIntoConstraints = false
            self.newInfoView.heightAnchor.constraint(equalToConstant: 0).isActive = true
            self.view.layoutIfNeeded()
        }
    }
    
    @objc private func onEventsHeaderTap() {
        AuthorizationCoordinator(navigationController).navigateToImportantEvents()
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        events?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell: ImportantEventsCollectionViewCell =
                importantEventsCollectionView
                .dequeueReusableCell(withReuseIdentifier: "importantEventsCollectionViewCell",
                                     for: indexPath)
                as? ImportantEventsCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        DispatchQueue.main.async {
            cell.addEventDate(date: self.events?[indexPath.row].date ?? 0)
            cell.addEventName(name: self.events?[indexPath.row].name ?? "???")
            cell.addEventImage(from: self.events?[indexPath.row].image ?? "???")
            cell.addAboutEvent(description: self.events?[indexPath.row].description ?? "???")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 40, height: collectionView.frame.height)
    }
}

extension HomeViewController: ButtonActionDelegate {
    func onButtonUserSettingsTa(sender: UIButton) {
        AuthorizationCoordinator(navigationController).navigateToEditProfile()
    }
}
