//
//  InfoViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 17.02.2021.
//

import UIKit

class InfoViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak private var communeCanalView: InfoView!
    @IBOutlet weak private var workingMailView: InfoView!
    @IBOutlet weak private var libraryView: InfoView!
    @IBOutlet weak private var passwordsView: InfoView!
    @IBOutlet weak private var cloudRefugeView: InfoView!
    @IBOutlet weak private var hrPoliticsView: InfoView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    private func setupUI() {
        self.view.backgroundColor = .background
        
        communeCanalView.setInfoHead(head: "Канали комунікації")
        communeCanalView.setInfoDescription(description: "Телеграм-канали, тощо...")
        communeCanalView.setInfotImage(image: "commune")
        
        workingMailView.setInfoHead(head: "Робоча пошта")
        workingMailView.setInfoDescription(description: "Реєстрація робочої пошти")
        workingMailView.setInfotImage(image: "mail")
        
        libraryView.setInfoHead(head: "Бібліотека")
        libraryView.setInfoDescription(description: "Електронні та паперові книги")
        libraryView.setInfotImage(image: "library")
        
        passwordsView.setInfoHead(head: "Паролі")
        passwordsView.setInfoDescription(description: "Загальні акаунти та паролі до них")
        passwordsView.setInfotImage(image: "passwords")
        
        cloudRefugeView.setInfoHead(head: "Хмарні сховища")
        cloudRefugeView.setInfoDescription(description: "Посилання на загальні хмари")
        cloudRefugeView.setInfotImage(image: "cloud")
        
        hrPoliticsView.setInfoHead(head: "HR політики")
        hrPoliticsView.setInfoDescription(description: "Посилання на загальні хмари")
        hrPoliticsView.setInfotImage(image: "hr_politics")
        
    }
}
