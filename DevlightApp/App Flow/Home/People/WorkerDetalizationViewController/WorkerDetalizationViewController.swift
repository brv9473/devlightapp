//
//  WorkerDetalizationViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 23.02.2021.
//

import UIKit
import Alamofire
import AlamofireImage

class WorkerDetalizationViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak private var workerAvatar: UIImageView!
    @IBOutlet weak private var workerName: WorkerDetalizationView!
    @IBOutlet weak private var workerLasname: WorkerDetalizationView!
    @IBOutlet weak private var workerPhoneNumber: WorkerDetalizationView!
    @IBOutlet weak private var workerBirth: WorkerDetalizationView!
    @IBOutlet weak private var workerPosition: WorkerDetalizationView!
    
    // MARK: - Properties
    
    var workerId: String?
    
    // MARK: - Private Properties
    
    private let service = Service()
    private var worker: ProfileResponseModel?
    private var positions: [PositionsModel]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        getWorkerById()
        getPositions()
        setupUI()
    }
    
    private func setupUI() {
        workerName.setInfo(text: "Ім'я:")
        workerLasname.setInfo(text: "Прізвище:")
        workerPhoneNumber.setInfo(text: "Номер телефону:")
        workerBirth.setInfo(text: "День народження:")
        workerPosition.setInfo(text: "Посада:")
    }
    
    private func getWorkerById() {
        service.getWorkerById(workerId: workerId ?? "???") { [weak self] worker in
            switch worker {
            case .success(let model):
                self?.worker = model
                self?.workerName.setWorkerInfo(text: self?.worker?.name ?? "???")
                self?.workerLasname.setWorkerInfo(text: self?.worker?.lastName ?? "???")
                self?.workerPhoneNumber.setWorkerInfo(text: self?.worker?.phoneNumber ?? "???")
                self?.workerBirth.setWorkerInfo(text: "\(self?.worker?.dateOfBirth ?? 00)")
                
                AF.request(self?.worker?.avatar ?? "???").responseImage { response in
                    switch response.result {
                    case.success(let image):
                        self?.workerAvatar.image = image
                        
                    case .failure(let error):
                        self?.showAlert(with: error)
                    }
                }
                
                if let pos = self?.positions?.filter({ (position) -> Bool in
                    position.positionId == self?.worker?.position
                }).first {
                    self?.workerPosition.setWorkerInfo(text: pos.name)
                }
                    
            case .failure(let error):
            self?.showAlert(with: error)
            }
        }
    }
    
    private func getPositions() {
        self.service.getPositions { [weak self] positions in
            switch positions {
            case .success(let model):
                self?.positions = model
                
            case .failure(let error):
                self?.positions = []
                self?.showAlert(with: error)
            }
        }
    }
}
