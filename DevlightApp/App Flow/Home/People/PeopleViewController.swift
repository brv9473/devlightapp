//
//  PeopleViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 17.02.2021.
//

import UIKit

class PeopleViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak private var peopleHeaderView: PeopleHeaderView!
    @IBOutlet weak private var positionsCollectionview: UICollectionView!
    @IBOutlet weak private var workersCollectionView: UICollectionView!
    
    // MARK: - Private Properties
    
    private let service = Service()
    private var positions: [PositionsModel]?
    private var workers: [FillInfoResponseModel]?
    private var resetWorkers: [FillInfoResponseModel]?
    private var selectedIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        workersCollectionView.delegate = self
        workersCollectionView.dataSource = self
        positionsCollectionview.delegate = self
        positionsCollectionview.dataSource = self
        peopleHeaderView.delegate = self
        peopleHeaderView.buttonActive = true
        
        positionsCollectionview.register(UINib(nibName: "PositionsCollectionViewCell", bundle: nil),
                                         forCellWithReuseIdentifier: "PositionsCollectionViewCell")
        workersCollectionView.register(UINib(nibName: "WorkersCollectionViewCell", bundle: nil),
                                       forCellWithReuseIdentifier: "WorkersCollectionViewCell")
        workersCollectionView.register(UINib(nibName: "WorkersListCollectionViewCell", bundle: nil),
                                       forCellWithReuseIdentifier: "WorkersListCollectionViewCell")
        
        showWorkers()
        getPositions()
        setupUI()
    }
    
    // MARK: - Private Methods
    
    private func setupUI() {
        self.view.backgroundColor = .background
        positionsCollectionview.backgroundColor = .background
        workersCollectionView.backgroundColor = .background
    }
    
    private func getPositions() {
        self.service.getPositions { [weak self] positions in
            switch positions {
            case .success(let model):
                self?.positions = model
                self?.positionsCollectionview.reloadData()
                
            case .failure(let error):
                self?.positions = []
                self?.showAlert(with: error)
            }
        }
    }
    
    private func showWorkers() {
        self.service.getWorkers { [weak self] workers in
            switch workers {
            case .success(let model):
                self?.workers = model
                self?.resetWorkers = model
                self?.peopleHeaderView.setCountPeople(count: "\(self?.workers?.count ?? 0)")
                self?.workersCollectionView.reloadData()
                
            case .failure(let error):
                self?.showAlert(with: error)
            }
        }
    }
}

extension PeopleViewController: UICollectionViewDelegate,
                                UICollectionViewDataSource,
                                UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == positionsCollectionview {
            return positions?.count ?? 0
            
        } else if collectionView == workersCollectionView {
            return workers?.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == positionsCollectionview {
            guard let cell: PositionsCollectionViewCell =
                    positionsCollectionview
                    .dequeueReusableCell(withReuseIdentifier: "PositionsCollectionViewCell",
                                         for: indexPath)
                    as? PositionsCollectionViewCell else {
                return UICollectionViewCell()
            }
            cell.setPositionName(name: positions?[indexPath.row].name ?? "???")
            return cell
            
        } else if collectionView == workersCollectionView {
            if peopleHeaderView.buttonActive! {
                guard let cell: WorkersCollectionViewCell =
                        workersCollectionView
                        .dequeueReusableCell(withReuseIdentifier: "WorkersCollectionViewCell",
                                             for: indexPath)
                        as? WorkersCollectionViewCell else {
                    return UICollectionViewCell()
                }
                
                if let pos = self.positions?.filter({ (position) -> Bool in
                    position.positionId == workers?[indexPath.row].position
                }).first {
                    cell.setWorkerPosition(position: pos.name)
                }
                cell.setWorkerName(name: workers?[indexPath.row].name ?? "???")
                cell.setWorkerLastName(lastName: workers?[indexPath.row].lastName ?? "???",
                                       isHidden: false)
                
                return cell
                
            } else {
                guard let cell: WorkersListCollectionViewCell =
                        workersCollectionView
                        .dequeueReusableCell(withReuseIdentifier: "WorkersListCollectionViewCell",
                                             for: indexPath)
                        as? WorkersListCollectionViewCell else {
                    return UICollectionViewCell()
                }
                if let pos = self.positions?.filter({ (position) -> Bool in
                    position.positionId == workers?[indexPath.row].position
                }).first {
                    cell.setWorkerPosition(position: pos.name)
                }
                let name = "\(workers?[indexPath.row].name ?? "???")" + " " +
                    "\(workers?[indexPath.row].lastName ?? "???")"
                cell.setWorkerName(name: name)
                return cell
            }
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == positionsCollectionview {
            let position = positions?[indexPath.row].positionId ?? "???"
            workers = resetWorkers
            
            let filteredWorkers = self.workers?.filter { worker in
                position == worker.position
            }
            
            workers?.removeAll()
            workers = filteredWorkers
            workersCollectionView.reloadData()
            
        } else if collectionView == workersCollectionView {
            if let workerId = workers?[indexPath.row].userId {
                _ = AuthorizationCoordinator(navigationController).navigateToWorkerDetalization(workerId: workerId)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == positionsCollectionview {
            return UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
            
        } else if collectionView == workersCollectionView {
            return UIEdgeInsets(top: 24, left: 12, bottom: 24, right: 12)
        }
        
        return UIEdgeInsets()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == workersCollectionView {
            if peopleHeaderView.buttonActive! {
                return CGSize(width: collectionView.frame.width / 2 - 25,
                              height: collectionView.frame.height / 3 + 44)
                
            } else {
                return CGSize(width: collectionView.frame.width - 50,
                              height: 48)
            }
        }
        return CGSize()
    }
}

extension PeopleViewController: PeopleHeaderButtonsDelegate {
    func onButtonCollectionTap(sender: UIButton) {
        peopleHeaderView.buttonActive = true
        workersCollectionView.reloadData()
    }
    
    func onButtonListTap(sender: UIButton) {
        peopleHeaderView.buttonActive = false
        workersCollectionView.reloadData()
    }
}
