//
//  EditProfileViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 16.02.2021.
//

import UIKit
import Alamofire

class EditProfileViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak private var userAvatar: UIImageView!
    @IBOutlet weak private var logOutButton: StyleButton!
    
    // MARK: - Properties
    
    private let service = Service()
    private var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap = UITapGestureRecognizer(target: self, action: #selector(avatarTap))
        userAvatar.addGestureRecognizer(tap)
        userAvatar.isUserInteractionEnabled = true
        
        addBarButtonItem()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    // MARK: - Private Methods
    
    private func setupUI() {
        self.view.backgroundColor = .background
        logOutButton.style = .active
    }
    
    private func addBarButtonItem() {
        navigationController?.navigationBar.barTintColor = .black
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "bar_button_item_back"),
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(goBackAction))
    }
    
    private func getProfile() {
        service.getProfile { [weak self] profile in
            switch profile {
            case .success(let model):
                print(model)
                
            case .failure(let error):
                self?.showAlert(with: error)
            }
        }
    }
    
    private func loadAvatar() {
    }
    
    private func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    imagePicker.allowsEditing = false
                    self.present(imagePicker, animated: true, completion: nil)
                } else {
                    let alert = UIAlertController(title: "Warning", message: "No camera find", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
    }
    
    private func openGellery() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                    imagePicker.allowsEditing = true
                    imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                    self.present(imagePicker, animated: true, completion: nil)
                } else {
                    let alert = UIAlertController(title: "Warning", message: "No Library find", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                }
    }
    
    @objc private func goBackAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func avatarTap() {
        let alert = UIAlertController(title: "Chose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (_) in
                    self.openCamera()
                }))
                
                alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (_) in
                    self.openGellery()
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
    }
}

extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
            if let pickedImage = info[.originalImage] as? UIImage {
                userAvatar.image = pickedImage
            }
            picker.dismiss(animated: true, completion: nil)
        }
}
