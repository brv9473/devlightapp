//
//  EventsDetalizationViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 16.02.2021.
//

import UIKit
import Alamofire
import AlamofireImage

class EventsDetalizationViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak private var image: UIImageView!
    @IBOutlet weak private var nameEvent: UILabel!
    @IBOutlet weak private var descriptionEvent: UILabel!
    
    // MARK: - Properties
    
    var eventId: String?
    private let service = Service()
    private var event: ResponseEventsModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getEventById()
        setupUI()
    }
    
    // MARK: - Private Methods
    
    private func setupUI() {
        self.view.backgroundColor = .background
    }
    
    private func getEventById() {
        service.getEventById(eventId: eventId ?? "????") { [weak self] event in
            switch event {
            case .success(let model):
                
                self?.addEventImage(from: model.image ?? "???")
                self?.nameEvent.text = model.name ?? "???"
                self?.descriptionEvent.text = model.description ?? "???"
                
            case .failure(let error):
                self?.showAlert(with: error)
            }
        }
    }
    
   private func addEventImage(from url: String) {
        AF.request(url).responseImage { response in
            if case .success(let image) = response.result {
                self.image.image = image
                }
        }
    }
    
}
