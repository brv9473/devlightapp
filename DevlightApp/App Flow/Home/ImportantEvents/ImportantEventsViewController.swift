//
//  ImportantEventsViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 16.02.2021.
//

import UIKit

class ImportantEventsViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak private var importentEventsCollectionView: UICollectionView!
    
    // MARK: - Private Properties
    
    private let service = Service()
    private var events: [ResponseEventsModel]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        importentEventsCollectionView.delegate = self
        importentEventsCollectionView.dataSource = self
        importentEventsCollectionView.register(UINib(nibName: "ImportantEventsCollectionViewCell", bundle: nil),
                                               forCellWithReuseIdentifier: "importantEventsCollectionViewCell")
        
        addBarButtonItem()
        showEvents()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    // MARK: - Private Methods
    
    private func setupUI() {
        self.view.backgroundColor = .background
        importentEventsCollectionView.backgroundColor = .background
    }
    
    private func addBarButtonItem() {
        navigationController?.navigationBar.barTintColor = .black
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "bar_button_item_back"),
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(goBackAction))
        navigationItem.leftBarButtonItem?.tintColor = .actionActive
        navigationItem.title = "Важливі події"
        
    }
    
    private func showEvents() {
        
        self.service.getEvents(parameters: RequestEventsModel(page: 0,
                                                              limit: 10)) { [weak self] events in
            switch events {
            case .success(let model):
                self?.events = model
                
                self?.importentEventsCollectionView.reloadData()
                
            case .failure(let error):
                self?.showAlert(with: error)
            }
        }
    }
    
    @objc private func goBackAction() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ImportantEventsViewController: UICollectionViewDelegate,
                                         UICollectionViewDataSource,
                                         UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        events?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell: ImportantEventsCollectionViewCell =
                importentEventsCollectionView
                .dequeueReusableCell(withReuseIdentifier: "importantEventsCollectionViewCell",
                                     for: indexPath)
                as? ImportantEventsCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        DispatchQueue.main.async {
            cell.addEventDate(date: self.events?[indexPath.row].date ?? 0)
            cell.addEventName(name: self.events?[indexPath.row].name ?? "???")
            cell.addEventImage(from: self.events?[indexPath.row].image ?? "???")
            cell.addAboutEvent(description: self.events?[indexPath.row].description ?? "???")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let eventId = events?[indexPath.row].eventId {
            _ = AuthorizationCoordinator(navigationController).navigateToEventsDetalization(eventId: eventId)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 48,
                      height: collectionView.frame.height / 3)
    }
}
