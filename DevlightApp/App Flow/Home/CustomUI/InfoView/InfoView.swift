//
//  InfoView.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 22.02.2021.
//

import UIKit
import Alamofire
import AlamofireImage

class InfoView: UIView {
    
    // MARK: - IBOutlets
    @IBOutlet weak private var contentView: UIView!
    @IBOutlet weak private var infoLabel: UILabel!
    @IBOutlet weak private var descriptionLabel: UILabel!
    @IBOutlet weak private var infoImage: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        guard let view = Bundle.main.loadNibNamed(String(describing: self.classForCoder),
                                                  owner: self,
                                                  options: nil)?.first as? UIView else { return }
        
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: self.leftAnchor),
            view.rightAnchor.constraint(equalTo: self.rightAnchor),
            view.heightAnchor.constraint(equalTo: self.heightAnchor),
            view.widthAnchor.constraint(equalTo: self.widthAnchor)
        ])
        
        setupUI()
    }
    
    private func setupUI() {
        contentView.layer.cornerRadius = 20
    }
    
    // MARK: - Methods
    
    func setInfoHead(head: String) {
        infoLabel.text = head
    }
    
    func setInfoDescription(description: String) {
        descriptionLabel.text = description
    }
    
    func setInfotImage(image: String) {
        infoImage.image = UIImage(named: image)
    }
}
