//
//  PeopleHeaderView.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 17.02.2021.
//

import UIKit

protocol PeopleHeaderButtonsDelegate: class {
    func onButtonCollectionTap(sender: UIButton)
    func onButtonListTap(sender: UIButton)
}

class PeopleHeaderView: UIView {

    // MARK: - IBOutlets
    
    @IBOutlet weak private var countPeopleLabel: UILabel!
    @IBOutlet weak private var peopleLabel: UILabel!
    @IBOutlet weak private var collectionButton: StyleButton!
    @IBOutlet weak private var tableButton: StyleButton!
    
    weak var delegate: PeopleHeaderButtonsDelegate?
    var buttonActive: Bool?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        guard let view = Bundle.main.loadNibNamed(String(describing: self.classForCoder),
                                                  owner: self,
                                                  options: nil)?.first as? UIView else { return }
        
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: self.leftAnchor),
            view.rightAnchor.constraint(equalTo: self.rightAnchor),
            view.heightAnchor.constraint(equalTo: self.heightAnchor),
            view.widthAnchor.constraint(equalTo: self.widthAnchor)
        ])
        
        setupUI()
    }
    
    private func setupUI() {
        collectionButton.tintColor = .actionActive
    }
    
    func setCountPeople(count: String) {
        countPeopleLabel.text = count
    }
    @IBAction func collectionAction(_ sender: Any) {
        delegate?.onButtonCollectionTap(sender: sender as? UIButton ?? UIButton())
        UIView.animate(withDuration: 0.2) {
            self.collectionButton.tintColor = .actionActive
            self.tableButton.tintColor = .white
        }
    }
    
    @IBAction func listAction(_ sender: Any) {
        delegate?.onButtonListTap(sender: sender as? UIButton ?? UIButton())
        UIView.animate(withDuration: 0.2) {
            self.tableButton.tintColor = .actionActive
            self.collectionButton.tintColor = .white
        }
    }
}
