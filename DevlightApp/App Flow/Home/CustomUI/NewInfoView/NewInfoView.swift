//
//  NewInfoView.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 01.02.2021.
//

import UIKit

class NewInfoView: UIView {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak private var contentView: UIView!
    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak private var firstInfoLabel: UILabel!
    @IBOutlet weak private var secondInfoLabel: UILabel!
    @IBOutlet weak private var closeButton: StyleButton!
    @IBOutlet weak private var goButton: StyleButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        guard let view = Bundle.main.loadNibNamed(String(describing: self.classForCoder),
                                                  owner: self,
                                                  options: nil)?.first as? UIView else { return }
        
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: self.leftAnchor),
            view.rightAnchor.constraint(equalTo: self.rightAnchor),
            view.heightAnchor.constraint(equalTo: self.heightAnchor),
            view.widthAnchor.constraint(equalTo: self.widthAnchor)
        ])
        
        setupUI()
    }
    
    private func setupUI() {
        contentView.backgroundColor = .homeViewColor
        contentView.layer.cornerRadius = 16
        closeButton.style = .inActive
        goButton.style = .active
    }
    
    func addActionForButtonClose(target: Any?, action: Selector, event: UIControl.Event) {
        closeButton.addTarget(target, action: action, for: event)
    }
    
    func addActionForButtonGo(target: Any?, action: Selector, event: UIControl.Event) {
        goButton.addTarget(target, action: action, for: event)
    }
}
