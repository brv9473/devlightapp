//
//  ImportantEventsHeaderView.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 01.02.2021.
//

import UIKit

class ImportantEventsHeaderView: UIView {
    
    // MARK: - IBOutlets
    @IBOutlet private weak var textLabel: UILabel!
    @IBOutlet private weak var vectorImage: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        guard let view = Bundle.main.loadNibNamed(String(describing: self.classForCoder),
                                                  owner: self,
                                                  options: nil)?.first as? UIView else { return }
        
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: self.leftAnchor),
            view.rightAnchor.constraint(equalTo: self.rightAnchor),
            view.heightAnchor.constraint(equalTo: self.heightAnchor),
            view.widthAnchor.constraint(equalTo: self.widthAnchor)
        ])
        
        setupUI()
    }
    
    private func setupUI() {
    }
    
    func addText(text: String) {
        textLabel.text = text
    }
}
