//
//  UserCardView.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 01.02.2021.
//

import UIKit

class UserCardView: UIView {
    
    // MARK: - IBOutlets
    @IBOutlet weak private var contentView: UIView!
    @IBOutlet weak private var logoWithTextImage: UIImageView!
    @IBOutlet weak private var userNumberCardLabel: UILabel!
    @IBOutlet weak private var leftTextView: UIView!
    @IBOutlet weak private var leftTextLabel: UILabel!
    @IBOutlet weak private var rightTextView: UIView!
    @IBOutlet weak private var rightTextLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        guard let view = Bundle.main.loadNibNamed(String(describing: self.classForCoder),
                                                  owner: self,
                                                  options: nil)?.first as? UIView else { return }
        
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: self.leftAnchor),
            view.rightAnchor.constraint(equalTo: self.rightAnchor),
            view.heightAnchor.constraint(equalTo: self.heightAnchor),
            view.widthAnchor.constraint(equalTo: self.widthAnchor)
        ])
        
        setupUI()
    }
    
    private func setupUI() {
        contentView.backgroundColor = .homeViewColor
        contentView.layer.cornerRadius = 16
        
        rightTextLabel.text = "DEVLIGHT"
        rightTextView.clipsToBounds = true
        rightTextView.layer.cornerRadius = 10
        rightTextView.layer.maskedCorners = [.layerMinXMaxYCorner,
                                             .layerMaxXMaxYCorner]
        
        leftTextView.clipsToBounds = true
        leftTextView.layer.cornerRadius = 10
        leftTextView.layer.maskedCorners = [.layerMaxXMaxYCorner,
                                            .layerMaxXMinYCorner]
        leftTextLabel.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
    }
    
    func addCardNumber(number: String) {
        userNumberCardLabel.text = number
    }
}
