//
//  Weekend.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 03.02.2021.
//

import UIKit

class WeekendView: UIView {

    // MARK: - IBOutlets
    @IBOutlet weak private var contentView: UIView!
    @IBOutlet weak private var headerView: ImportantEventsHeaderView!
    
    @IBOutlet weak private var weekendImage: UIImageView!
    @IBOutlet weak private var weekendLabel: UILabel!
    @IBOutlet weak private var usedDaysLabel: UILabel!
    @IBOutlet weak private var canUseDaysLabel: UILabel!
    
    @IBOutlet weak private var workFromHomeImage: UIImageView!
    @IBOutlet weak private var workFromHomeLabel: UILabel!
    @IBOutlet weak private var workFromHomeUsedDays: UILabel!
    @IBOutlet weak private var workFromHomeCanUseDays: UILabel!
    
    @IBOutlet weak private var hospitalWorkImage: UIImageView!
    @IBOutlet weak private var hospitalWorkLabel: UILabel!
    @IBOutlet weak private var hospitalWorkUsedDays: UILabel!
    @IBOutlet weak private var hospitalWorkCanUseDays: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        guard let view = Bundle.main.loadNibNamed(String(describing: self.classForCoder),
                                                  owner: self,
                                                  options: nil)?.first as? UIView else { return }
        
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: self.leftAnchor),
            view.rightAnchor.constraint(equalTo: self.rightAnchor),
            view.heightAnchor.constraint(equalTo: self.heightAnchor),
            view.widthAnchor.constraint(equalTo: self.widthAnchor)
        ])
        
        setupUI()
    }
    
    private func setupUI() {
        contentView.layer.cornerRadius = 16
        contentView.layer.masksToBounds = true
        headerView.addText(text: "Вихідні дні")
    }
    
    func addVacationDays(days: String) {
        canUseDaysLabel.text = "/\(days)"
    }
    
    func addVacationUsedDays(days: String) {
        usedDaysLabel.text = days
    }
    
    func addSickLeaveDays(days: String) {
        hospitalWorkCanUseDays.text = "/\(days)"
    }
    
    func addSickLeaveUsedDays(days: String) {
        hospitalWorkUsedDays.text = days
    }
    
    func addRemotesDays(days: String) {
        workFromHomeCanUseDays.text = "/\(days)"
    }
    
    func addRemotesUsedDays(days: String) {
        workFromHomeUsedDays.text = days
    }
}
