//
//  ButtonMenuView.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 03.02.2021.
//

import UIKit

protocol ButtonsActionDelegate: class {
    func onButtonMainTap(sender: UIButton)
    func onButtonPeopleTap(sender: UIButton)
    func onButtonProjectsTap(sender: UIButton)
    func onButtonInfoTap(sender: UIButton)
}

class ButtonMenuView: UIView {

// MARK: - IBOutlets
    
    @IBOutlet weak private var contentView: UIView!
    @IBOutlet weak private var mainButton: StyleButton!
    @IBOutlet weak private var peopleButton: StyleButton!
    @IBOutlet weak private var projectsButton: StyleButton!
    @IBOutlet weak private var infoButton: StyleButton!
    
    // MARK: - Properties
    
    weak var delegate: ButtonsActionDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        guard let view = Bundle.main.loadNibNamed(String(describing: self.classForCoder),
                                                  owner: self,
                                                  options: nil)?.first as? UIView else { return }
        
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: self.leftAnchor),
            view.rightAnchor.constraint(equalTo: self.rightAnchor),
            view.heightAnchor.constraint(equalTo: self.heightAnchor),
            view.widthAnchor.constraint(equalTo: self.widthAnchor)
        ])
        
        setupUI()
    }
    
    private func setupUI() {        
        mainButton.style = .active
        peopleButton.style = .inActiveHome
        projectsButton.style = .inActiveHome
        infoButton.style = .inActiveHome
        
        layer.cornerRadius = 20
        layer.masksToBounds = true
    }
    
    @IBAction func mainAction(_ sender: Any) {
        delegate?.onButtonMainTap(sender: sender as? UIButton ?? UIButton())
       _ = mainButton.isSelected ? mainButton.style == .active : mainButton.style == .inActiveHome
    }
    
    @IBAction func peopleAction(_ sender: Any) {
        delegate?.onButtonPeopleTap(sender: sender as? UIButton ?? UIButton())
        _ = peopleButton.isSelected ? peopleButton.style == .active : peopleButton.style == .inActiveHome
    }
    
    @IBAction func projectsAction(_ sender: Any) {
        delegate?.onButtonProjectsTap(sender: sender as? UIButton ?? UIButton())
        _ = projectsButton.isSelected ? projectsButton.style == .active : projectsButton.style == .inActiveHome
    }
    
    @IBAction func infoAction(_ sender: Any) {
        delegate?.onButtonInfoTap(sender: sender as? UIButton ?? UIButton())
        _ = infoButton.isSelected ? infoButton.style == .active : infoButton.style == .inActiveHome
    }
}
