//
//  ImportantEventsCollectionViewCell.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 01.02.2021.
//

import UIKit
import Alamofire
import AlamofireImage

class ImportantEventsCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak private var backView: UIView!
    @IBOutlet weak private var eventImage: UIImageView!
    @IBOutlet weak private var eventDateLabel: UILabel!
    @IBOutlet weak private var eventName: UILabel!
    @IBOutlet weak private var aboutEvent: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 16
    }
    
    func addEventDate(date: Int) {
        let date = Date.init(timeIntervalSince1970: TimeInterval(date) / 1000)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yy"
        eventDateLabel.text = formatter.string(from: date)
    }
    
    func addEventName(name: String) {
        eventName.text = name
    }
    
    func addAboutEvent(description: String) {
        aboutEvent.text = description
    }
    
    func addEventImage(from url: String) {
        eventImage.layer.cornerRadius = 8
        
        AF.request(url).responseImage { response in
            if case .success(let image) = response.result {
                self.eventImage.image = image
                }
        }
    }
}
