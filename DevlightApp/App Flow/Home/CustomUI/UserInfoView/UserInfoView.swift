//
//  UserInfoView.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 27.01.2021.
//

import UIKit

protocol ButtonActionDelegate: class {
    func onButtonUserSettingsTa(sender: UIButton)
}

class UserInfoView: UIView {
    
    // MARK: - IBOutlets
    @IBOutlet weak private var userImageView: UIImageView!
    @IBOutlet weak private var userInfoLabel: UILabel!
    @IBOutlet weak private var userPositionLabel: UILabel!
    @IBOutlet weak private var userSettingsButton: UIButton!
    
    // MARK: - Properties
    
    weak var delegate: ButtonActionDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        guard let view = Bundle.main.loadNibNamed(String(describing: self.classForCoder),
                                                  owner: self,
                                                  options: nil)?.first as? UIView else { return }
        
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: self.leftAnchor),
            view.rightAnchor.constraint(equalTo: self.rightAnchor),
            view.heightAnchor.constraint(equalTo: self.heightAnchor),
            view.widthAnchor.constraint(equalTo: self.widthAnchor)
        ])
        
        setupUI()
    }
    
    private func setupUI() {
        userImageView.isCircular(true)
    }
    
    func addUserInfo(nameAndLastName: String) {
        userInfoLabel.text = nameAndLastName
    }
    
    func addUserPosition(position: String) {
        userPositionLabel.text = position
    }
    @IBAction func userSettingsAction(_ sender: Any) {
        delegate?.onButtonUserSettingsTa(sender: sender as? UIButton ?? UIButton())
    }
}
