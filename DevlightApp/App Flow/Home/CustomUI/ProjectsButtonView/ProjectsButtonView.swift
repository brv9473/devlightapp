//
//  ProjectsButtonView.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 22.02.2021.
//

import UIKit

protocol ProjectsButtonsDelegate: class {
    func allProjectsTap(sender: UIButton)
    func myProjectsTap(sender: UIButton)
}

class ProjectsButtonView: UIView {
    
    // MARK: - IBOutlets
    @IBOutlet weak private var contentView: UIView!
    @IBOutlet weak private var allProjectsButton: UIButton!
    @IBOutlet weak private var myProjectsButton: UIButton!
    
    // MARK: - Properties
    
    weak var delegate: ProjectsButtonsDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        guard let view = Bundle.main.loadNibNamed(String(describing: self.classForCoder),
                                                  owner: self,
                                                  options: nil)?.first as? UIView else { return }
        
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: self.leftAnchor),
            view.rightAnchor.constraint(equalTo: self.rightAnchor),
            view.heightAnchor.constraint(equalTo: self.heightAnchor),
            view.widthAnchor.constraint(equalTo: self.widthAnchor)
        ])
        
        setupUI()
    }
    
    private func setupUI() {
        allProjectsButton.layer.cornerRadius = 18
        myProjectsButton.layer.cornerRadius = 18
        contentView.layer.cornerRadius = 18
        myProjectsButton.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        contentView.backgroundColor = .actionInactive
    }
    @IBAction func allProjectsAction(_ sender: Any) {
        UIView.animate(withDuration: 0.2) {
            self.myProjectsButton.backgroundColor = .actionInactive
            self.allProjectsButton.backgroundColor = .actionActive
        }
        delegate?.allProjectsTap(sender: sender as? UIButton ?? UIButton())
    }
    
    @IBAction func myProjectsAction(_ sender: Any) {
        UIView.animate(withDuration: 0.2) {
            self.allProjectsButton.backgroundColor = .actionInactive
            self.myProjectsButton.backgroundColor = .actionActive
        }
        myProjectsButton.layer.maskedCorners = [
            .layerMaxXMaxYCorner,
            .layerMaxXMinYCorner,
            .layerMinXMaxYCorner,
            .layerMinXMinYCorner]
        
        delegate?.myProjectsTap(sender: sender as? UIButton ?? UIButton())
    }
    
}
