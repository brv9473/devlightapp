//
//  WorkersCollectionViewCell.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 18.02.2021.
//

import UIKit
import Alamofire
import AlamofireImage

class WorkersCollectionViewCell: UICollectionViewCell {

    // MARK: - IBOutlets
    
    @IBOutlet weak private var workerContentView: UIView!
    @IBOutlet weak private var workerImage: UIImageView!
    @IBOutlet weak private var workerName: UILabel!
    @IBOutlet weak private var moreButton: UIButton!
    @IBOutlet weak private var workerLastname: UILabel!
    @IBOutlet weak private var workerPosition: UILabel!
    @IBOutlet weak private var nameAndLastnameStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        workerImage.layer.cornerRadius = 16
    }

    func setWorkerAvatar(from url: String) {
        AF.request(url).responseImage { response in
            if case .success(let image) = response.result {
                self.workerImage.image = image
                }
        }
    }
    
    func setWorkerName(name: String) {
        workerName.text = name
    }
    
    func setWorkerLastName(lastName: String?, isHidden: Bool) {
        workerLastname.text = lastName
        if isHidden {
            nameAndLastnameStackView.removeArrangedSubview(workerLastname)
            nameAndLastnameStackView.spacing = 50
        }
    }
    
    func setWorkerPosition(position: String) {
        workerPosition.text = position
    }
}
