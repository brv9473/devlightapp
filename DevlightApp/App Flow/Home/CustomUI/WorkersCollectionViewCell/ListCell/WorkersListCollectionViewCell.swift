//
//  WorkersListCollectionViewCell.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 19.02.2021.
//

import UIKit
import Alamofire
import AlamofireImage

class WorkersListCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak private var workerAvatar: UIImageView!
    @IBOutlet weak private var workerName: UILabel!
    @IBOutlet weak private var workerPosition: UILabel!
    @IBOutlet weak private var moreInfoButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setWorkerAvatar(from url: String) {
        AF.request(url).responseImage { response in
            if case .success(let image) = response.result {
                self.workerAvatar.image = image
                }
        }
    }
    
    func setWorkerName(name: String) {
        workerName.text = name
    }
    
    func setWorkerPosition(position: String) {
        workerPosition.text = position
    }
}
