//
//  CareerView.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 03.02.2021.
//

import UIKit

class CareerView: UIView {

    // MARK: - IBOutlets
    @IBOutlet weak private var careerLabel: UILabel!
    @IBOutlet weak private var userPosition: UILabel!
    
    @IBOutlet weak private var backgroundViewForLvl: UIView!
    @IBOutlet weak private var lvlImage: UIImageView!
    @IBOutlet weak private var lvlLabel: UILabel!
    
    @IBOutlet weak private var backgroundViewForSkillAssessment: UIView!
    @IBOutlet weak private var imageSkillAssessment: UIImageView!
    @IBOutlet weak private var labelSkillAssessment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        guard let view = Bundle.main.loadNibNamed(String(describing: self.classForCoder),
                                                  owner: self,
                                                  options: nil)?.first as? UIView else { return }
        
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: self.leftAnchor),
            view.rightAnchor.constraint(equalTo: self.rightAnchor),
            view.heightAnchor.constraint(equalTo: self.heightAnchor),
            view.widthAnchor.constraint(equalTo: self.widthAnchor)
        ])
        
        setupUI()
    }
    
    private func setupUI() {
        backgroundViewForLvl.layer.cornerRadius = 16
        backgroundViewForSkillAssessment.layer.cornerRadius = 16
        layer.cornerRadius = 16
        layer.masksToBounds = true
    }
    
    func addUserPosition(position: String) {
        userPosition.text = position
    }
}
