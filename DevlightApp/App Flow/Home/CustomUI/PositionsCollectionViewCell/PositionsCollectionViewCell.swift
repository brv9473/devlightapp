//
//  PositionsCollectionViewCell.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 17.02.2021.
//

import UIKit

class PositionsCollectionViewCell: UICollectionViewCell {

    // MARK: - IBOutlets
    
    @IBOutlet weak private var positionContentView: UIView!
    @IBOutlet weak private var imagePostion: UIImageView!
    @IBOutlet weak private var position: UILabel!
    
    override var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? .actionActive : .actionInactive
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 16
    }
    
   func setPositionImage(image: String) {
    imagePostion.image = UIImage(named: image)
    }
    
    func setPositionName(name: String) {
        position.text = name
    }
}
