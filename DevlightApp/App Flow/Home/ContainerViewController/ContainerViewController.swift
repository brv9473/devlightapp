//
//  ContainerViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 26.02.2021.
//

import UIKit

class ContainerViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak private var containerView: UIView!
    @IBOutlet weak private var menuButtonView: ButtonMenuView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        menuButtonView.delegate = self
    }
    
    func addViewController(viewController: UIViewController) {
        addChild(viewController)
        view.addSubview(viewController.view)
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
        
    }
    
    func removeViewcontroller(viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
}

extension ContainerViewController: ButtonsActionDelegate {
    func onButtonMainTap(sender: UIButton) {
        AuthorizationCoordinator(navigationController).navigateToHome()
    }
    
    func onButtonPeopleTap(sender: UIButton) {
        AuthorizationCoordinator(navigationController).navigateToPeople()

    }
    
    func onButtonProjectsTap(sender: UIButton) {
        AuthorizationCoordinator(navigationController).navigateToProjects()
    }
    
    func onButtonInfoTap(sender: UIButton) {
        AuthorizationCoordinator(navigationController).navigateToInfo()
    }
}
