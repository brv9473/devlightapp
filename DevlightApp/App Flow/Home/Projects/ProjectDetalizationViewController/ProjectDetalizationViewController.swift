//
//  ProjectDetalizationViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 23.02.2021.
//

import UIKit

class ProjectDetalizationViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak private var logoProject: UIImageView!
    @IBOutlet weak private var nameProject: UILabel!
    @IBOutlet weak private var descriptionProject: WorkerDetalizationView!
    @IBOutlet weak private var dateOfStartProject: WorkerDetalizationView!
    @IBOutlet weak private var statusProject: WorkerDetalizationView!
    @IBOutlet weak private var workersProject: WorkerDetalizationView!
    @IBOutlet weak private var appleLinkProject: StyleButton!
    @IBOutlet weak private var androidLinkProject: StyleButton!
    
    // MARK: - Properties
    var projectId: String?
    
    // MARK: - Private Properties
    private let service = Service()
    private var project: ProjectDetalizationModel?
    private var workers: [FillInfoResponseModel]?
    private var appleLink: String?
    private var androidLink: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showWorkers()
        getProjectById()
        setupUI()
    }
    
    private func setupUI() {
        appleLinkProject.style = .active
        androidLinkProject.style = .active
        
        descriptionProject.setInfo(text: "Опис")
        dateOfStartProject.setInfo(text: "Початок розробки")
        workersProject.setInfo(text: "Розробники")
        statusProject.setInfo(text: "Статус")
    }
    
    private func showWorkers() {
        self.service.getWorkers { [weak self] workers in
            switch workers {
            case .success(let model):
                self?.workers = model
                
            case .failure(let error):
                self?.showAlert(with: error)
            }
        }
    }
    
    private func getProjectById() {
        service.getProjectById(projectId: projectId ?? "???") { [weak self] project in
            switch project {
            case .success(let model):
                self?.project = model
                self?.appleLink = self?.project?.platforms?.ios?.link
                self?.androidLink = self?.project?.platforms?.android?.link
                self?.nameProject.text = self?.project?.name
                self?.descriptionProject.setWorkerInfo(text: self?.project?.description ?? "???")
                self?.statusProject.setWorkerInfo(text: self?.project?.status ?? "???")
                self?.dateOfStartProject.setWorkerInfo(text:
                                                        self?.convertDate(date:
                                                                            self?.project?.dateOfStart ?? 0) ?? "???")
                for worker in self!.project!.workers! {
                    if let work = self?.workers?.filter({ (workMan) -> Bool in
                        worker == workMan.userId
                    }).first {
                        self?.workersProject.setWorkerInfo(text: "\(work.name ?? "???")"
                                                            + " "
                                                            + "\(work.lastName ?? "???")")
                    }
                }
                
                if self?.project?.platforms == nil ||
                    self?.project?.platforms?.android?.link == "" ||
                    self?.project?.platforms?.ios?.link == "" {
                    
                    self?.appleLinkProject.isHidden = true
                    self?.androidLinkProject.isHidden = true
                }
                
            case .failure(let error):
                self?.showAlert(with: error)
            }
        }
    }
    
    func convertDate(date: Int) -> String {
        let myDate: String?
        let date = Date.init(timeIntervalSince1970: TimeInterval(date) / 1000)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yy"
        myDate = formatter.string(from: date)
        
        return myDate ?? "???"
    }
    
    @IBAction func appleActionLinkProject(_ sender: Any) {
        UIApplication.shared.open(NSURL(string: appleLink ?? "???")! as URL)
    }
    
    @IBAction func androidActionLinkProject(_ sender: Any) {
        UIApplication.shared.open(NSURL(string: androidLink ?? "???")! as URL)
    }
}
