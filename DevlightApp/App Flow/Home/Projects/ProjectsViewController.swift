//
//  ProjectsViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 17.02.2021.
//

import UIKit

class ProjectsViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak private var countProjectLabel: UILabel!
    @IBOutlet weak private var projectsLabel: UILabel!
    @IBOutlet weak private var buttonsView: ProjectsButtonView!
    @IBOutlet weak private var projectsCollectionView: UICollectionView!
    
    // MARK: - Private Properties
    
    private let service = Service()
    private var projects: [ResponseProjectModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        projectsCollectionView.delegate = self
        projectsCollectionView.dataSource = self
        buttonsView.delegate = self
        
        projectsCollectionView.register(UINib(nibName: "WorkersCollectionViewCell", bundle: nil),
                                       forCellWithReuseIdentifier: "WorkersCollectionViewCell")
        
        showProjects()
        setupUI()
    }
    
    // MARK: - Private Methods
    
    private func setupUI() {
        self.view.backgroundColor = .background
        projectsCollectionView.backgroundColor = .background
    }
    
    private func showProjects() {
        service.getProjects(parameters: RequestProjectsModel(page: 0, limit: 4)) { [weak self] projects in
            switch projects {
            case .success(let model):
                self?.projects = model
                self?.countProjectLabel.text = "\(self?.projects?.count ?? 00)"
                self?.projectsCollectionView.reloadData()
                
            case .failure(let error):
                self?.showAlert(with: error)
            }
        }
    }
    
    private func showMyprojects() {
        service.getMyProjects(parameters: RequestProjectsModel(page: 0,
                                                               limit: 4)) { [weak self] projects in
            switch projects {
            case .success(let model):
                self?.projects = model
                
                self?.countProjectLabel.text = "\(self?.projects?.count ?? 00)"
                self?.projectsCollectionView.reloadData()
                
            case .failure(let error):
                self?.showAlert(with: error)
            }
        }
    }
}

extension ProjectsViewController: UICollectionViewDelegate,
                                  UICollectionViewDataSource,
                                  UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return projects?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: WorkersCollectionViewCell =
                projectsCollectionView
                .dequeueReusableCell(withReuseIdentifier: "WorkersCollectionViewCell",
                                     for: indexPath)
                as? WorkersCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.setWorkerLastName(lastName: nil, isHidden: true)
        cell.setWorkerName(name: projects?[indexPath.row].name ?? "???")
        cell.setWorkerAvatar(from: projects?[indexPath.row].icon ?? "???")
        cell.setWorkerPosition(position: projects?[indexPath.row].description ?? "???")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let projectId = projects?[indexPath.row].projectId {
            _ = AuthorizationCoordinator(navigationController).navigateToProjectDetalization(projectId: projectId)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width / 2 - 10,
                      height: collectionView.frame.height / 2 - 70)
    }
}

extension ProjectsViewController: ProjectsButtonsDelegate {
    func allProjectsTap(sender: UIButton) {
        projects?.removeAll()
        showProjects()
        projectsCollectionView.reloadData()
    }
    
    func myProjectsTap(sender: UIButton) {
        projects?.removeAll()
        showMyprojects()
        projectsCollectionView.reloadData()
    }
}
