//
//  ChoosePositionViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 20.10.2020.
//

import UIKit
import Alamofire

final class ChoosePositionViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet private weak var positionLabel: UILabel!
    @IBOutlet private weak var positionTableView: UITableView!
    
    // MARK: - Properties
    private var positions: [PositionsModel]?
    private let service = Service()
    private let debouncer = Debouncer(timeInterval: 2)
    var completion: ((PositionsModel?) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .background
        
        positionTableView.delegate = self
        positionTableView.dataSource = self
        
        positionTableView.backgroundColor = .background
        positionTableView.register(UINib(nibName: "PositionTableViewCell", bundle: nil),
                                   forCellReuseIdentifier: "PositionTableViewCell")
        
        showPositions()
    }
    
    private func showPositions() {
        self.showSpinner(onView: self.view)
        self.debouncer.renewInterval()
        
        self.debouncer.handler = {
            self.service.getPositions { [weak self] positions in
                switch positions {
                case .success(let model):
                    self?.positions = model
                    
                    if let userPositionModel = try? JSONEncoder().encode(model) {
                        UserDefaults.standard.setValue(userPositionModel, forKey: "userPosition")
                    }
                    
                case .failure(let error):
                    self?.positions = []
                    self?.showAlert(with: error)
                }
                self?.positionTableView.reloadData()
                self?.removeSpinner()
            }
        }
    }
}

extension ChoosePositionViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return positions?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PositionTableViewCell",
                                                       for: indexPath) as? PositionTableViewCell else {
            return UITableViewCell()
        }
        
        cell.addtextLabel(text: positions?[indexPath.row].name ?? "0")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        completion?(positions?[indexPath.row])
        
        tableView.deselectRow(at: indexPath, animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}
