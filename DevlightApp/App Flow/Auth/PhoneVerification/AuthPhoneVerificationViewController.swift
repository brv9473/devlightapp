//
//  AuthPhoneVerificationViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 19.10.2020.
//

import UIKit
import Alamofire

final class AuthPhoneVerificationViewController: UIViewController {
    
    @IBOutlet  private weak var registrstionLabel: UITextField!
    @IBOutlet  private weak var phoneNumberTextField: BaseTextField!
    @IBOutlet  private weak var continueButton: StyleButton!
    
    // MARK: - Properties
    private var buttonConstraint: NSLayoutConstraint?
    private let service = Service()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberTextField.delegate = self
        
        continueButton.addTarget(self, action: #selector(continueButtonTap), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        
        addBarButtonItem()
        setupContBottonCnstr()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        if launchedBefore {
//            navigationItem.leftBarButtonItem = nil
        }
    }
    
    private func setupUI() {
        self.view.backgroundColor = .background
        
        phoneNumberTextField.fieldType = .phone
        
        continueButton.style = .inActive
        continueButton.isEnabled = false
        continueButton.setTitle("Продовжити", for: .normal)
    }
    
    private func setupContBottonCnstr() {
        continueButton.translatesAutoresizingMaskIntoConstraints = false
        
        let btnCnstr = continueButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,
                                                         constant: -10)
        NSLayoutConstraint.activate([
            continueButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 16),
            continueButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -16),
            btnCnstr
        ])
       
        buttonConstraint = btnCnstr
    }
    
    private func addBarButtonItem() {
        navigationController?.navigationBar.barTintColor = .black
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "bar_button_item_back"),
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(goBackAction))
    }
    
//    private func displaySmsCodeScreen() {
//        let number = phoneNumberTextField.text?.replacingOccurrences(of: " ", with: "")
//        let userPhoneNumber = "+380" + "\(number ?? "0")"
//        let storyboard = UIStoryboard(name: "Auth", bundle: nil)
//
//        if let smsCodeVC = storyboard.instantiateViewController(withIdentifier: "SmsCodeViewController")
//            as? SmsCodeViewController {
//            smsCodeVC.phoneNumber = userPhoneNumber
//            navigationController?.pushViewController(smsCodeVC, animated: true)
//
//        } else {
//            print("Error -SmsCodeViewController ")
//        }
//        print("PHONE - \(userPhoneNumber)")
//    }
    
     func format(with mask: String, phone: String) -> String {
        let numbers = phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        var result = ""
        var index = numbers.startIndex

        for character in mask where index < numbers.endIndex {
            if character == "X" {
                result.append(numbers[index])
                index = numbers.index(after: index)

            } else {
                result.append(character)
            }
        }
        return result
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo
        
        if let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardSize.cgRectValue.height
            buttonConstraint?.constant = -(keyboardHeight)

        } else {
            print("Error - keyboardSize")
        }
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        buttonConstraint?.constant = -16

        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
     }
    
    @objc private func goBackAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func continueButtonTap() {
        let number = phoneNumberTextField.text?.replacingOccurrences(of: " ", with: "")
        let userPhoneNumber = "+380" + "\(number ?? "0")"
        
        service.postPhoneNumber(params: PhoneNumberModel(phoneNumber: userPhoneNumber),
                                completion: .some({ [weak self] (result) in
                                    switch result {
                                    case .success:
                                        let smsCodeVC = AuthorizationCoordinator(self?.navigationController)
                                            .navigateToSmsCode()
                                        smsCodeVC.phoneNumber = userPhoneNumber
                                        
                                    case .failure(let error):
                                        self?.showAlert(with: error)
                                    }}))
    }
}

extension AuthPhoneVerificationViewController: UITextFieldDelegate {
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if phoneNumberTextField.text?.count ?? 0 < 11 {
            UIView.animate(withDuration: 0.3) {
                self.continueButton.style = .inActive
                self.continueButton.isEnabled = false
            }
       
        } else {
            UIView.animate(withDuration: 0.3) {
                self.continueButton.style = .active
                self.continueButton.isEnabled = true
            }
        }
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return false
        }
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        textField.text = format(with: "XX XX XXXXX", phone: newString)
        return false
    }
}
