//
//  RegistrtionViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 20.10.2020.
//

import UIKit

final class RegistrtionViewController: UIViewController, UITextFieldDelegate {

    // MARK: - IBOutlets
    @IBOutlet private weak var registrtionLabel: UILabel!
    @IBOutlet private weak var firstNameTextField: BaseTextField!
    @IBOutlet private weak var lastNameTextField: BaseTextField!
    @IBOutlet private weak var choosePositionView: PositionView!
    @IBOutlet private weak var teamLeadView: TeamLeadView!
    @IBOutlet private weak var continueButton: StyleButton!
        
    // MARK: - Properties
    private var buttonConstraint: NSLayoutConstraint?
    private let service = Service()
    private var postionId: String?
    private var teamLead: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        continueButton.addTarget(self, action: #selector(continueButtonTap), for: .touchUpInside)
        
        firstNameTextField.addTarget(self, action: #selector(valueChanget(_:)), for: .editingChanged)
        lastNameTextField.addTarget(self, action: #selector(valueChanget(_:)), for: .editingChanged)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        
        setupContBottonCnstr()
        addTapGesture()
        addBarButtonItem()
        setupUI()
    }
    
    // MARK: - Private Methods
    private func setupUI() {
        self.view.backgroundColor = .background
        
        continueButton.style = .inActive
        continueButton.setTitle("Продовжити", for: .normal)
        
        firstNameTextField.fieldType = .firstName
        lastNameTextField.fieldType = .lastName
        
        choosePositionView.layer.masksToBounds = true
        choosePositionView.layer.cornerRadius = 13
    }
    
    private func setupContBottonCnstr() {
        continueButton.translatesAutoresizingMaskIntoConstraints = false

        let btnCnstr = continueButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,
                                                         constant: -10)

        NSLayoutConstraint.activate([
            continueButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 16),
            continueButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -16),
            btnCnstr
        ])
        
        buttonConstraint = btnCnstr
    }
    
    private func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(showPositions))
        choosePositionView.addGestureRecognizer(tap)
    }
    
    private func addBarButtonItem() {
        navigationController?.navigationBar.barTintColor = .black
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "bar_button_item_back"),
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(goBackAction))
    }
    
    private func displayLogOutScreen() {
        AuthorizationCoordinator(navigationController).navigateToLogOut()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo
        if let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardSize.cgRectValue.height
            buttonConstraint?.constant = -(keyboardHeight)

        } else {
            print("Error - keyboardSize")
        }
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        buttonConstraint?.constant = -16

        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
     }
    
    @objc private func goBackAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func continueButtonTap() {
        teamLead = teamLeadView.isOn
        
        service.postFillInfo(params: FillInfoModel(name: firstNameTextField.text ?? "0",
                                                   lastName: lastNameTextField.text ?? "0",
                                                   position: postionId ?? "0",
                                                   teamLead: teamLead), completinon: .some({ [weak self] (result) in
                                                    switch result {
                                                    case .success(let model):
                                                        if let encodedUserModel = try? JSONEncoder().encode(model) {
                                                            UserDefaults.standard.setValue(encodedUserModel,
                                                                                           forKey: "userModel")
                                                            AuthorizationCoordinator(self?.navigationController)
                                                                .navigateToContainer()
                                                        }
                                                        
                                                    case .failure(let error):
                                                        self?.showAlert(with: error)
                                                    }
                                                   }))        
    }
    
    @objc private func showPositions() {
        let positionVC = AuthorizationCoordinator(navigationController)
            .navigateToChoosePosition()
        
            positionVC.completion = { [weak self] in
                self?.choosePositionView.addTextLabel(text: $0?.name ?? "0")
                    self?.postionId = $0?.positionId
                
                if $0?.positionId != nil,
                   self?.firstNameTextField.text?.count ?? 0 >= 3,
                   self?.lastNameTextField.text?.count ?? 0 >= 3 {
                    UIView.animate(withDuration: 0.3) {
                        self?.continueButton.style = .active
                    }
                    
                } else {
                    UIView.animate(withDuration: 0.3) {
                        self?.continueButton.style = .inActive
                    }
                }
            }
    }
    
    @objc func valueChanget(_ textField: UITextField) {
        if firstNameTextField.text?.count ?? 0 >= 3,
           lastNameTextField.text?.count ?? 0 >= 3,
           postionId != nil {
            UIView.animate(withDuration: 0.3) {
                self.continueButton.style = .active
            }
            
        } else {
            UIView.animate(withDuration: 0.3) {
                self.continueButton.style = .inActive
            }
        }
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let characterSet = CharacterSet.letters
        return string.rangeOfCharacter(from: characterSet.inverted) != nil ? false : true
    }
}
