//
//  ViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 15.10.2020.
//

import UIKit

final class AuthViewController: UIViewController {

    // MARK: - IBOutlets

    @IBOutlet private weak var logoImage: UIImageView!
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var buttonsStackView: UIStackView!
    @IBOutlet private weak var missButton: StyleButton!
    @IBOutlet private weak var continueButton: StyleButton!
    @IBOutlet private weak var indicatorView: IndicatorView!
    
    // MARK: - Private Properties
    private var contentDict = [String: String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        takeInfoFromPlist()

        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(UINib(nibName: "ContentCollectionViewCell", bundle: nil),
                                forCellWithReuseIdentifier: "contentCell")
        
        missButton.addTarget(self, action: #selector(missAction), for: .touchUpInside)
        continueButton.addTarget(self, action: #selector(continueAction), for: .touchUpInside)
        
        indicatorView.addIndicator(indicatorCount: contentDict.count)
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    // MARK: - Private Methods
    
    private func setupUI() {
        self.view.backgroundColor = .background
        
        collectionView.backgroundColor = .clear
        collectionView.isPagingEnabled = true
        
        missButton.style = .inActive
        missButton.setTitle("Пропустити", for: .normal)
        
        continueButton.setTitle("Продовжити", for: .normal)
        continueButton.style = .active
    }
    
    private func takeInfoFromPlist() {
        if let path = Bundle.main.path(forResource: "Onbording", ofType: "plist"),
           let myDict = NSDictionary(contentsOfFile: path) {
            contentDict = myDict as? [String: String] ?? [:]
            collectionView.reloadData()
        }
    }
    
    private func displayAuthPhoneScreen() {
        let storyboard = UIStoryboard(name: "Auth", bundle: nil)
        if let phoneVC =
            storyboard.instantiateViewController(withIdentifier: "AuthPhoneVerificationViewController")
            as? AuthPhoneVerificationViewController {
            navigationController?.pushViewController(phoneVC, animated: true)

        } else {
            print("Error - AuthPhoneVerificationViewController")
        }
    }
    
    private func showBeginButton() {
        UIView.animate(withDuration: 0.3) {
            self.missButton.alpha = 0
            self.continueButton.setTitle("Розпочати", for: .normal)
            self.missButton.isHidden = true
        }
    }
    
    private func showContinueButton() {
        UIView.animate(withDuration: 0.3) {
            self.missButton.alpha = 1
            self.continueButton.setTitle("Продовжити", for: .normal)
            self.missButton.isHidden = false
        }
    }
    
    // MARK: - Actions
    @objc func missAction() {
        AuthorizationCoordinator(navigationController).navigateToPhoneInput()
    }
    
    @objc func continueAction() {
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
        let visibleIndexPathRow = visibleIndexPath?.row ?? 0 + 1
        
        if visibleIndexPath?.row ?? 0 < 3 {
            collectionView.scrollToNextItem()
        }
        
        if visibleIndexPath?.row == 2 {
            showBeginButton()
            
        } else if visibleIndexPath?.row == 3 {
            AuthorizationCoordinator(navigationController).navigateToPhoneInput()
        }
        
        indicatorView.visibleIndicator(indexPath: visibleIndexPathRow)
    }
}

extension AuthViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contentDict.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell: ContentCollectionViewCell =
                collectionView.dequeueReusableCell(withReuseIdentifier: "contentCell", for: indexPath)
                as? ContentCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        if let path = Bundle.main.path(forResource: "Onbording", ofType: "plist") {
            if let dict = NSDictionary(contentsOfFile: path) as? [String: String] {
            for (key, value) in dict {
                cell.addImage(image: key)
                cell.addTextLabel(text: value)
            }
          }
        }
        
        return cell
    }

     func collectionView(_ collectionView: UICollectionView,
                         layout collectionViewLayout: UICollectionViewLayout,
                         sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
        
        if visibleIndexPath?.row == 3 {
            showBeginButton()

        } else {
            showContinueButton()
        }
        
        indicatorView.visibleIndicator(indexPath: visibleIndexPath?.row ?? 0)
    }
}
