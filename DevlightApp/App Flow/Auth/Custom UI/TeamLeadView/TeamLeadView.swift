//
//  TeamLeadView.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 20.10.2020.
//

import UIKit

class TeamLeadView: UIView {
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var textLabel: UILabel!
    @IBOutlet private weak var switchButton: UISwitch!

    var isOn: Bool {
        return switchButton.isOn
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setup()
    }
    
    private func setup() {
        guard let view = Bundle.main.loadNibNamed(String(describing: self.classForCoder),
                                                  owner: self,
                                                  options: nil)?.first as? UIView else { return }
        
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: self.leftAnchor),
            view.rightAnchor.constraint(equalTo: self.rightAnchor),
            view.heightAnchor.constraint(equalTo: self.heightAnchor),
            view.widthAnchor.constraint(equalTo: self.widthAnchor)
        ])
        
        setupUI()
    }
    
    private func setupUI() {
        backgroundColor = .background
        switchButton.tintColor = .switchOff
        switchButton.backgroundColor = .switchOff
        switchButton.clipsToBounds = true
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        switchButton.layer.cornerRadius = switchButton.frame.height / 2
    }
}
