//
//  ContentCollectionViewCell.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 16.10.2020.
//

import UIKit

class ContentCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var contentImage: UIImageView!
    @IBOutlet private weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func addImage(image: String) {
        contentImage.image = UIImage(named: image)
    }
    
    func addTextLabel(text: String) {
        contentLabel.text = text
    }
}
