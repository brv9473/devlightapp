//
//  AuthButton.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 15.10.2020.
//

import UIKit

class StyleButton: BaseButton {
    
    enum Style {
        case none
        case active
        case inActive
        case inActiveHome
    }
    
    // MARK: - Properties
    
    var style: Style = .none {
        didSet { updateByStyle() }
    }
    
    // MARK: - Private Methods

    private func updateByStyle() {
        switch style {
        case .active: active()
        case .inActive: nonActive()
        case .inActiveHome: inActiveHome()
            
        case .none: break
        }
    }
    
    private func active() {
        backgroundColor = .actionActive
        setTitleColor(.white, for: .normal)
        isRounded = true
    }
    
    private func nonActive() {
        backgroundColor = .actionInactive
        setTitleColor(.white, for: .normal)
        isRounded = true
    }
    
    private func inActiveHome() {
        backgroundColor = .clear
        setTitleColor(.white, for: .normal)
        isRounded = true
    }
}
