//
//  BaseButton.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 15.10.2020.
//

import UIKit

class BaseButton: UIButton {
    
    // MARK: - Properties
    
    var isRounded: Bool = false {
        didSet {
            roundedButton()
        }
    }
    
    // MARK: - Methods

    private func roundedButton() {
        layer.cornerRadius = isRounded ? frame.height / 2 : 0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        roundedButton()
    }
}
