//
//  IndecatorView.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 16.10.2020.
//

import UIKit

class IndicatorView: UIView {
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        Bundle.main.loadNibNamed("IndicatorView", owner: self, options: nil)
        addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        addConstraints()
    }
    
    func addIndicator(indicatorCount: Int) {
        
        for indicator in 0..<indicatorCount {
            let indicatorView = UIView()
            indicatorView.frame = CGRect(x: 0, y: 0, width: 6, height: 6)
            indicatorView.backgroundColor = .actionInactive
            indicatorView.isCircular(true)
            indicatorView.translatesAutoresizingMaskIntoConstraints = false
            indicatorView.heightAnchor.constraint(equalToConstant: 6).isActive = true
            stackView.addArrangedSubview(indicatorView)
            
            if indicator == 0 {
                indicatorView.backgroundColor = .white
            }
        }
    }
    
    func visibleIndicator(indexPath: Int) {
        for (index, view) in stackView.subviews.enumerated() {
            if index == indexPath {
                UIView.animate(withDuration: 0.3) {
                    view.backgroundColor = .white
                }
            } else {
                UIView.animate(withDuration: 0.3) {
                    view.backgroundColor = .actionInactive
                }
            }
        }
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: contentView.topAnchor),
            rightAnchor.constraint(equalTo: contentView.rightAnchor),
            bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            leftAnchor.constraint(equalTo: contentView.leftAnchor)
        ])
    }
}
