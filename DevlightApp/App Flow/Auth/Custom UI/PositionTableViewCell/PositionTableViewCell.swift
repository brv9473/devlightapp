//
//  PositionTableViewCell.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 20.10.2020.
//

import UIKit

class PositionTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    
    @IBOutlet private weak var positionLabel: UILabel!
    @IBOutlet private weak var vectorImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setup()
    }
    
    private func setup() {
        backgroundColor = .background
    }
    
    func addtextLabel(text: String) {
        positionLabel.text = text
    }
    
}
