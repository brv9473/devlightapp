//
//  BaseTextField.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 19.10.2020.
//

import UIKit

class BaseTextField: UITextField {
    
    // MARK: - Properties
    
    var isRounded: Bool = false
    var isRoundedForSmsCode: Bool = false
    
    var maxTextCount: Int? {
        didSet {
            if maxTextCount != nil {
                addTarget(self, action: #selector(textAction), for: .editingChanged)
            } else {
                removeTarget(self, action: #selector(textAction), for: .editingChanged)
            }
        }
    }
    
    // MARK: - Types
    
    enum FieldType {
        case firstName, lastName, smsCode, phone
        case none
    }
    
    // MARK: - Properties
    
    var fieldType: FieldType = .none {
        didSet { setupFieldType() }
    }
    
    // MARK: - Private Methods
    
    private func setupFieldType() {
        keyboardAppearance = .dark
        tintColor = .actionActive
        
        switch fieldType {
        case .firstName:
            keyboardType = .default
            autocapitalizationType = .sentences
            maxTextCount = 20
            attributedPlaceholder = NSAttributedString(string: "Ім'я",
                                                       attributes:
                                                        [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            layer.cornerRadius = 13
            isRoundedForSmsCode = true
            
        case .lastName:
            keyboardType = .default
            autocapitalizationType = .sentences
            maxTextCount = 20
            attributedPlaceholder = NSAttributedString(string: "Прізвище",
                                                       attributes:
                                                        [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            layer.cornerRadius = 13
            isRoundedForSmsCode = true
            
        case .smsCode:
            keyboardType = .numberPad
            maxTextCount = 1
            layer.cornerRadius = 13
            isRoundedForSmsCode = true

        case .phone:
            keyboardType = .numberPad
            maxTextCount = 11
            layer.cornerRadius = frame.height / 2
            isRounded = true
            attributedPlaceholder = NSAttributedString(string: "Номер телефону",
                                                       attributes:
                                                        [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            setLeftIcon()

        case .none:
            break
        }
        setupTextField()
    }
    
    private func setupTextField() {
        textColor = .white
        font = UIFont(name: "Pragmatica", size: 16)
        backgroundColor = .actionInactive
        layer.masksToBounds = true
    }
    
    private func onlyChar() {
        text = text?.filter({ $0.isLetter })
    }
    
    private func setLeftIcon() {
        let myLeftView = UIView(frame: CGRect(x: 0, y: 0, width: 75, height: 15))
        
        let imageViewFlag = UIImageView(frame: CGRect(x: 0, y: 0, width: 21, height: 15))
        let image = UIImage(named: "flag_ua")
        imageViewFlag.image = image
        
        let countryPhoneCodeLabel = UILabel(frame: CGRect(x: 30, y: 0, width: 50, height: 15))
        countryPhoneCodeLabel.text = "+380"
        countryPhoneCodeLabel.font = UIFont(name: "Pragmatica", size: 16)
        countryPhoneCodeLabel.textColor = .white
        countryPhoneCodeLabel.textAlignment = .center
        
        myLeftView.addSubview(imageViewFlag)
        myLeftView.addSubview(countryPhoneCodeLabel)
        
        leftView = myLeftView
        leftViewMode = .always
        
    }
    
    @objc private func textAction() {
        guard let maxTextCount = maxTextCount,
              let newText = text,
              newText.count > maxTextCount
        else { return }
        
        text = String(newText.prefix(maxTextCount))
    }
    
    // MARK: - Methods

    override func deleteBackward() {
        if let text = self.text, text.isEmpty {
            _ = delegate?.textField?(self, shouldChangeCharactersIn: .init(), replacementString: "")
            
        } else {
            super.deleteBackward()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if isRounded { layer.cornerRadius = frame.height / 2 }
        if isRoundedForSmsCode { layer.cornerRadius = 13 }
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        super.leftViewRect(forBounds: bounds).offsetBy(dx: 16, dy: 0)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.textRect(forBounds: bounds)
        
        if fieldType == .smsCode {
            rect.origin = .init(x: 8, y: rect.origin.y)
            
        } else if rect.origin.x < 16 {
            rect.origin = .init(x: 16, y: rect.origin.y)
        }
        return rect
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.editingRect(forBounds: bounds)
        
        if fieldType == .smsCode {
            rect.origin = .init(x: 8, y: rect.origin.y)
            
        } else if rect.origin.x < 16 {
            rect.origin = .init(x: 16, y: rect.origin.y)
        }
        return rect
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return super.placeholderRect(forBounds: bounds)
    }
}
