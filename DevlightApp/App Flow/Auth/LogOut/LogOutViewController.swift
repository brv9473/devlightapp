//
//  LogOutViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 09.12.2020.
//

import UIKit

final class LogOutViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBarButtonItem()
    }
    
    // MARK: - Private Methods
    private func addBarButtonItem() {
        navigationController?.navigationBar.barTintColor = .black
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "bar_button_item_back"),
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(goBackAction))
    }
    
    // MARK: - Private Methods
    private func displayAuthScreen() {
        AuthorizationCoordinator(navigationController).navigateToOnboarding()
    }
    
    @objc private func goBackAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Actions

    @IBAction func logOutAction(_ sender: Any) {
        let domain = Bundle.main.bundleIdentifier ?? "Error - domain"
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
        displayAuthScreen()
    }
}
