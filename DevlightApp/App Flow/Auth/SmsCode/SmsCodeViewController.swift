//
//  SmsCodeViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 20.10.2020.
//

import UIKit
import Alamofire

final class SmsCodeViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet private weak var smsCodeLabel: UILabel!
    
    @IBOutlet private weak var firstTextField: BaseTextField!
    @IBOutlet private weak var secondTextField: BaseTextField!
    @IBOutlet private weak var thirdTextField: BaseTextField!
    @IBOutlet private weak var fourthTextField: BaseTextField!
    @IBOutlet private weak var fifthTextField: BaseTextField!
    @IBOutlet private weak var sixthTextField: BaseTextField!
    
    @IBOutlet private weak var infoSmsLabel: UILabel!
    @IBOutlet private weak var phoneLabel: UILabel!
    @IBOutlet private weak var continueButton: StyleButton!
    
    @IBOutlet private weak var resendSmsCodeButton: UIButton!
    @IBOutlet private weak var timerLabel: UILabel!
    
    // MARK: - Properties
    private var buttonConstraint: NSLayoutConstraint?
    private let phoneController = AuthPhoneVerificationViewController()
    private let service = Service()
    private var countDownTimer = Timer()
    private var totalTime = 59
    private var textFieldArray: [BaseTextField] = []
    var phoneNumber: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldArray = [
            firstTextField,
            secondTextField,
            thirdTextField,
            fourthTextField,
            fifthTextField,
            sixthTextField
        ]
        
        for (index, value) in textFieldArray.enumerated() {
            value.delegate = self
            value.fieldType = .smsCode
            
            value.tag = index
        }
        
        continueButton.addTarget(self, action: #selector(continueButtonTap), for: .touchUpInside)
        resendSmsCodeButton.addTarget(self, action: #selector(resendSms), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        
        startTimer()
        setupContBottonCnstr()
        addBarButtonItem()
        setupUI()
    }
    
    func getPhoneNumber(_ phone: String) {
        self.phoneLabel.text = phone
    }
    
    // MARK: - Private Methods
    
    private func setupUI() {
        self.view.backgroundColor = .background
        
        continueButton.style = .inActive
        continueButton.isEnabled = false
        continueButton.setTitle("Продовжити", for: .normal)
        
        phoneLabel.text = phoneNumber
    }
    
    private func setupContBottonCnstr() {
        continueButton.translatesAutoresizingMaskIntoConstraints = false
        
        let btnCnstr = continueButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,
                                                              constant: -10)
        
        NSLayoutConstraint.activate([
            continueButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 16),
            continueButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -16),
            btnCnstr
        ])
        
        buttonConstraint = btnCnstr
    }
    
    private func addBarButtonItem() {
        navigationController?.navigationBar.barTintColor = .black
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "bar_button_item_back"),
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(goBackAction))
    }
    
    private func startTimer() {
        countDownTimer.invalidate()
        countDownTimer = Timer.scheduledTimer(timeInterval: 1,
                                              target: self,
                                              selector: #selector(updateTimer),
                                              userInfo: nil,
                                              repeats: true)
    }
    
    private  func endTimer() {
        countDownTimer.invalidate()
    }
    
    private func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        
        return String(format: "%02d", seconds)
    }
    
    private func displayRegistrationScreen() {
        AuthorizationCoordinator(navigationController).navigateToRegistration()
    }
    
    private func getSmsCode() -> String {
        textFieldArray.reduce("") { $0 + ($1.text ?? "0") }
    }
    
    private func updateContinueButtonState() {
        let smsCode = getSmsCode()
        let isActive = smsCode.count == 6
        
        UIView.animate(withDuration: 0.3) {
            self.continueButton.style = isActive ? .active : .inActive
            self.continueButton.isEnabled = isActive
        }
    }
    
    private func activateNextTextField(of textField: UITextField) {
        let nextIndex = textField.tag + 1
        
        if nextIndex < textFieldArray.count {
            textFieldArray[nextIndex].becomeFirstResponder()
            textField.textColor = .lightGray
        }
    }
    
    private func activatePreviousTextField(of textField: UITextField) {
        let previousIndex = textField.tag - 1
        
        if previousIndex >= 0 {
            textFieldArray[previousIndex].becomeFirstResponder()
            textField.textColor = .white
        }
    }
    
    @objc private func updateTimer() {
        timerLabel.text = "\(timeFormatted(totalTime))"
        
        if totalTime != 0 {
            totalTime -= 1
            resendSmsCodeButton.isEnabled = false
            
        } else {
            UIView.animate(withDuration: 0.3) {
                self.resendSmsCodeButton.setTitleColor(.actionActive, for: .normal)
                self.timerLabel.alpha = 0
            }
            
            resendSmsCodeButton.isEnabled = true
            endTimer()
        }
    }
    
    @objc private func resendSms() {
        totalTime = 59
        let number = (phoneLabel.text?.replacingOccurrences(of: " ", with: "")) ?? "alert dobavyty"
        
        UIView.animate(withDuration: 0.3) {
            self.timerLabel.alpha = 1
            self.resendSmsCodeButton.setTitleColor(.white, for: .normal)
        }
        
        startTimer()
        service.postResendSmsCode(params: PhoneNumberModel(phoneNumber: number))
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo
        
        if let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardSize.cgRectValue.height
            buttonConstraint?.constant = -(keyboardHeight)
            
        } else {
            print("Error - keyboardSize")
        }
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        buttonConstraint?.constant = -16
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc private func goBackAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func continueButtonTap() {
        let number = phoneLabel.text?.replacingOccurrences(of: " ", with: "")
        let smsCode = getSmsCode()
        
        print("__Number : \(number ?? "0")")
        print("__SmsCode: \(smsCode)")
        
        service.postVerifySmsCode(params: SmsCodeModel(phoneNumber: number ?? "0",
                                                       code: Int(smsCode) ?? 0),
                                  completinon: .some({ [weak self] (result) in
                                    switch result {
                                    case .success(let model):
                                        let defaults = UserDefaults.standard
                                        defaults.setValue(model.token, forKey: "token")
                                        defaults.setValue(model.refreshToken,
                                                          forKey: "refreshToken")
                                        self?.displayRegistrationScreen()
                                        
                                    case .failure(let error):
                                        self?.showAlert(with: error)
                                    }
                                  }))
    }
}

// TextField extension
extension SmsCodeViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        
        if string.isEmpty {
            activatePreviousTextField(of: textField)
            textField.text = ""
            
        } else {
            activateNextTextField(of: textField)
            textField.text = string
        }
        
        updateContinueButtonState()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.textColor = .white
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.textColor = .lightGray
    }
}
