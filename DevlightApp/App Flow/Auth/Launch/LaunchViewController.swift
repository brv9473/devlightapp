//
//  LaunchViewController.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 08.12.2020.
//

import UIKit

final class LaunchViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstInitialization()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    // MARK: - Private Methods
    
    private func firstInitialization() {
        let defaults = UserDefaults.standard
        let token = defaults.string(forKey: "token")
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
//        AuthorizationCoordinator(navigationController).navigateToOnboarding()
//        AuthorizationCoordinator(navigationController).navigateToRegistration()
//        AuthorizationCoordinator(navigationController).navigateToHome()
        if launchedBefore {
            if token != nil {
                if let decodedData = defaults.object(forKey: "userModel") as? Data {
                    if let userModel = try? JSONDecoder().decode(FillInfoResponseModel.self, from: decodedData) {
                        print(userModel)
                        AuthorizationCoordinator(navigationController).navigateToHome()
//                        AuthorizationCoordinator(navigationController).navigateToContainer()

                    } else {
                        AuthorizationCoordinator(navigationController).navigateToRegistration()
                    }

                } else {
                    AuthorizationCoordinator(navigationController).navigateToRegistration()
                }

            } else {
                AuthorizationCoordinator(navigationController).navigateToPhoneInput()
            }

        } else {
            AuthorizationCoordinator(navigationController).navigateToOnboarding()
            UserDefaults.standard.set(true, forKey: "launchedBefore")
        }
    }
}
