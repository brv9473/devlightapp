//
//  Helpers.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 15.10.2020.
//

import UIKit

extension UIColor {
    
    static var background: UIColor {
        UIColor(named: "Background Color") ?? black
    }
    
    static var actionActive: UIColor {
        UIColor(named: "Action Active Color") ?? black
    }
    
    static var actionInactive: UIColor {
        UIColor(named: "Action Inactive Color") ?? black
    }
    
    static var switchOff: UIColor {
        UIColor(named: "Switch Off Color") ?? black
    }
    
    static var homeViewColor: UIColor {
        UIColor(named: "Home View Color") ?? black
    }
    
    static var userCardLogoTextColor: UIColor {
        UIColor(named: "User Card Logo Text Color ") ?? .black
    }
}
