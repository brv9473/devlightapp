//
//  Extensions.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 16.10.2020.
//

import UIKit

extension UIView {
    
    func isCircular(_ rounded: Bool) {
        if rounded {
            self.layer.cornerRadius = self.frame.width / 2
        }
    }
}

var vSpinner: UIView?

extension UIViewController {
    func showSpinner(onView: UIView) {
        let spinnerView = UIView(frame: onView.bounds)
        spinnerView.backgroundColor = .background
        let activityIndicator = UIActivityIndicatorView(style: .white)
        activityIndicator.color = .white
        activityIndicator.startAnimating()
        activityIndicator.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(activityIndicator)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
    
    func showAlert(with error: Error?) {
        let errorMessage =
            (error as? ErrorResponseModel)?.localizedDescription ?? error?.localizedDescription ?? "No description"
        
        let alertVC = UIAlertController(title: "Oops",
                                        message: errorMessage,
                                        preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertVC.addAction(okAction)
        present(alertVC, animated: true, completion: nil)
    }
}

extension UICollectionView {
    func scrollToNextItem() {
        let contentOffset = CGFloat(floor(self.contentOffset.y + self.bounds.size.height))
        self.moveToFrame(contentOffset: contentOffset)
    }
    
    func moveToFrame(contentOffset: CGFloat) {
            self.setContentOffset(CGPoint(x: self.contentOffset.x, y: contentOffset), animated: true)
        }
}
