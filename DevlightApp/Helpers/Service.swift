//
//  Service.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 23.10.2020.
//

import Foundation
import Alamofire

class Service {
    
    // MARK: - Auth
    
    func getPositions(completion: ((Result<[PositionsModel], Error>) -> Void)?) {
        let endPoint = "positions"
    
        ApiManager.sendGet(endPoint: endPoint, parameters: nil, headers: nil, completion: completion)
    }
    
    func postPhoneNumber(params: PhoneNumberModel, completion: ((Result<EmptyResponseModel, Error>) -> Void)?) {
        let endPoint = "auth"
        
        ApiManager.sendPostWithResponse(endPoint: endPoint, headers: nil, parameters: params, completion: completion)
    }
    
    func postVerifySmsCode(params: SmsCodeModel, completinon: ((Result<ResponseSmsCodeModel, Error>) -> Void)?) {
        let endPoint = "verifyCode"
        
        ApiManager.sendPostWithResponse(endPoint: endPoint,
                                        headers: nil,
                                        parameters: params,
                                        completion: completinon)
    }
    
    func postResendSmsCode(params: PhoneNumberModel) {
        let endPoint = "resendCode"
        
        ApiManager.sendPost(endPoint: endPoint, parameters: params)
    }
    
    func postFillInfo(params: FillInfoModel, completinon: ((Result<FillInfoResponseModel, Error>) -> Void)?) {
        let endPoint = "profile/fill_info"
        let token = UserDefaults.standard.string(forKey: "token")
        let headers = HTTPHeaders.init(["Authorization": token ?? "Token nil"])
        
        ApiManager.sendPostWithResponse(endPoint: endPoint,
                                        headers: headers,
                                        parameters: params,
                                        completion: completinon)
    }
    
    func refreshToken(params: RefreshTokenModel, completinon: ((Result<ResponseSmsCodeModel, Error>) -> Void)?) {
        let endPoint = "refresh"
        
        ApiManager.sendPostWithResponse(endPoint: endPoint,
                                        headers: nil,
                                        parameters: params,
                                        completion: completinon)
    }
    
    // MARK: - Home

    func getEvents(parameters: RequestEventsModel, completion: ((Result<[ResponseEventsModel], Error>) -> Void)?) {
        let endPoint = "events"
        let token = UserDefaults.standard.string(forKey: "token")
        let headers = HTTPHeaders.init(["Authorization": token ?? "Token nil"])
        
        ApiManager.sendGetWithResponse(endPoint: endPoint,
                                       headers: headers,
                                       parameters: parameters,
                                       completion: completion)
    }
    
    func getEventById(eventId: String, completion: ((Result<ResponseEventByIdModel, Error>) -> Void)?) {
        
        let endPoint = "event/\(eventId)"
        let token = UserDefaults.standard.string(forKey: "token")
        let headers = HTTPHeaders.init(["Authorization": token ?? "Token nil"])
        
        ApiManager.sendGetWithResponse(endPoint: endPoint,
                                       headers: headers,
                                       parameters: EmptyParametersModel.init(),
                                       completion: completion)
    }
    
    func getMyVacation(completion: ((Result<MyVacation, Error>) -> Void)?) {
        let endPoint = "vacations"
        let token = UserDefaults.standard.string(forKey: "token")
        let headers = HTTPHeaders.init(["Authorization": token ?? "Token nil"])
        
        ApiManager.sendGet(endPoint: endPoint, parameters: nil, headers: headers, completion: completion)
    }
    
    func getWorkers(completion: ((Result<[FillInfoResponseModel], Error>) -> Void)?) {
        let endPoint = "workers"
        let token = UserDefaults.standard.string(forKey: "token")
        let headers = HTTPHeaders.init(["Authorization": token ?? "Token nil"])
        
        ApiManager.sendGet(endPoint: endPoint,
                           headers: headers,
                           completion: completion)
    }
    
    func getWorkerById(workerId: String, completion: ((Result<ProfileResponseModel, Error>) -> Void)?) {
        let endPoint = "worker/\(workerId)"
        let token = UserDefaults.standard.string(forKey: "token")
        let headers = HTTPHeaders.init(["Authorization": token ?? "Token nil"])
        
        ApiManager.sendGetWithResponse(endPoint: endPoint,
                                       headers: headers,
                                       parameters: EmptyParametersModel.init(),
                                       completion: completion)
    }
    
    func getProjects(parameters: RequestProjectsModel,
                     completion: ((Result<[ResponseProjectModel], Error>) -> Void)?) {
        let endPoint = "projects"
        let token = UserDefaults.standard.string(forKey: "token")
        let headers = HTTPHeaders.init(["Authorization": token ?? "Token nil"])
        
        ApiManager.sendGetWithResponse(endPoint: endPoint,
                                       headers: headers,
                                       parameters: parameters,
                                       completion: completion)
    }
    
    func getMyProjects(parameters: RequestProjectsModel,
                       completion: ((Result<[ResponseProjectModel], Error>) -> Void)?) {
        let endPoint = "projects/my"
        let token = UserDefaults.standard.string(forKey: "token")
        let headers = HTTPHeaders.init(["Authorization": token ?? "Token nil"])
        
        ApiManager.sendGetWithResponse(endPoint: endPoint,
                                       headers: headers,
                                       parameters: parameters,
                                       completion: completion)
    }
    
    func getProjectById(projectId: String, completion: ((Result<ProjectDetalizationModel, Error>) -> Void)?) {
        let endPoint = "project/\(projectId)"
        let token = UserDefaults.standard.string(forKey: "token")
        let headers = HTTPHeaders.init(["Authorization": token ?? "Token nil"])
        
        ApiManager.sendGetWithResponse(endPoint: endPoint,
                                       headers: headers,
                                       parameters: EmptyParametersModel.init(),
                                       completion: completion)
    }
    
    func getProfile(completion: ((Result<ProfileResponseModel, Error>) -> Void)?) {
        let endPoint = "profile"
        let token = UserDefaults.standard.string(forKey: "token")
        let headers = HTTPHeaders.init(["Authorization": token ?? "Token nil"])
        
        ApiManager.sendGet(endPoint: endPoint, parameters: nil, headers: headers, completion: completion)
    }
}
