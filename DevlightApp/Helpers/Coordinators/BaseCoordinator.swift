//
//  BaseCoordinator.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 21.12.2020.
//

import UIKit

class BaseCoordinator {
    
    let navigation: UINavigationController?

    init(_ navigation: UINavigationController?) {
        self.navigation = navigation
    }
}
