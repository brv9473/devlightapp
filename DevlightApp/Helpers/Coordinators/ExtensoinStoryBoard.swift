//
//  ExtensoinStoryBoard.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 21.12.2020.
//

import UIKit

enum Storyboard: String, CaseIterable {
    case auth = "AuthViewController"
    case phoneInput = "AuthPhoneVerificationViewController"
    case smsCode = "SmsCodeViewController"
    case registration = "RegistrtionViewController"
    case positions = "ChoosePositionViewController"
    case home = "HomeViewController"
    case logOut = "LogOutViewController"
    case editProfile = "EditProfileViewController"
    case importantEvents = "ImportantEventsViewController"
    case eventsDetalization = "EventsDetalizationViewController"
    case people = "PeopleViewController"
    case projects = "ProjectsViewController"
    case info = "InfoViewController"
    case workerDetalization = "WorkerDetalizationViewController"
    case projectDetalization = "ProjectDetalizationViewController"
    case container = "ContainerViewController"
}

extension Storyboard {
    var viewControllers: [String] {
        switch self {
        case .auth:
            return [AuthViewController.name]
        case .phoneInput:
            return [AuthPhoneVerificationViewController.name]
        case .smsCode:
            return [SmsCodeViewController.name]
        case .registration:
            return [RegistrtionViewController.name]
        case .positions:
            return [ChoosePositionViewController.name]
        case .logOut:
            return [LogOutViewController.name]
        case .home:
            return [HomeViewController.name]
        case .editProfile:
            return [EditProfileViewController.name]
        case .importantEvents:
            return [ImportantEventsViewController.name]
        case .eventsDetalization:
            return [EventsDetalizationViewController.name]
        case .people:
            return [PeopleViewController.name]
        case .projects:
            return [ProjectsViewController.name]
        case .info:
            return [InfoViewController.name]
        case .workerDetalization:
            return [WorkerDetalizationViewController.name]
        case .projectDetalization:
            return [ProjectDetalizationViewController.name]
        case .container:
            return [ContainerViewController.name]
        }
    }
}

extension Storyboard {
    func instantiateInitialController() -> UIViewController {
        guard let vcToInit = UIStoryboard(name: self.rawValue, bundle: nil)
                .instantiateInitialViewController() else {
            fatalError("Storyboard doesn't have initial controller or there's no storyboard with this name")
        }
        return vcToInit
    }
}

extension Storyboard {
    static func value(byViewController viewController: UIViewController.Type) -> Storyboard {
        return value(byViewController: viewController.name)
    }
    
    static func value(byViewController viewControllerName: String) -> Storyboard {
        for storyboard in Storyboard.allCases
            where storyboard.viewControllers.contains(viewControllerName) {
                return storyboard
        }
        
        fatalError("ViewController: \"\(viewControllerName)\" is not defined in Storyboard enum")
    }
}

protocol Instantiable: class {}

extension Instantiable where Self: UIViewController {
    static var instantiate: Self {
        return UIViewController.instantiate(vcName: self.name)
    }
}

extension UIViewController: Instantiable {
    static var name: String {
        return String(describing: self)
    }
    
    static func instantiate<Instantiable: UIViewController>(vcName: String) -> Instantiable {
        let storyboard = UIStoryboard(name: Storyboard.value(byViewController: vcName).rawValue,
                                      bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: vcName)
                as? Instantiable else {
            fatalError("ViewController: \"\(vcName)\" incorrect cast to \(Instantiable.self)")
        }
        
        return viewController
    }
}
