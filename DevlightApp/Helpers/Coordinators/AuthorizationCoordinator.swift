//
//  AuthorizationCoordinator.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 21.12.2020.
//

import UIKit

final class AuthorizationCoordinator: BaseCoordinator {
    // MARK: - Auth
    func navigateToOnboarding(animated: Bool = true) {
        let onboardingVC = AuthViewController.instantiate
        navigation?.setViewControllers([onboardingVC], animated: animated)
    }
    
    func navigateToPhoneInput(animated: Bool = true) {
        let phoneInputVC = AuthPhoneVerificationViewController.instantiate
        navigation?.pushViewController(phoneInputVC, animated: animated)
    }
    
    func navigateToSmsCode(animated: Bool = true) -> SmsCodeViewController {
        let smsCodeInputVC = SmsCodeViewController.instantiate
        navigation?.pushViewController(smsCodeInputVC, animated: animated)
        
        return smsCodeInputVC
    }
    
    func navigateToRegistration(animated: Bool = true) {
        let registrationVC = RegistrtionViewController.instantiate
        navigation?.pushViewController(registrationVC, animated: animated)
    }
    
    func navigateToChoosePosition(animated: Bool = true) -> ChoosePositionViewController {
        let choosePositionVC = ChoosePositionViewController.instantiate
        navigation?.present(choosePositionVC, animated: animated, completion: nil)
        
        return choosePositionVC
    }
    
    // MARK: - Home
    
    func navigateToHome(animated: Bool = true) {
        let homeVC = HomeViewController.instantiate
        navigation?.pushViewController(homeVC, animated: animated)
    }
    
    func navigateToLogOut(animated: Bool = true) {
        let logOutVC = LogOutViewController.instantiate
        navigation?.pushViewController(logOutVC, animated: animated)
    }
    
    func navigateToEditProfile(animated: Bool = true) {
        let editProfileVC = EditProfileViewController.instantiate
        navigation?.pushViewController(editProfileVC, animated: animated)
    }
    
    func navigateToImportantEvents(animated: Bool = true) {
        let importantEventsVC = ImportantEventsViewController.instantiate
        navigation?.pushViewController(importantEventsVC, animated: animated)
    }
    
    func navigateToEventsDetalization(animated: Bool = true, eventId: String) -> EventsDetalizationViewController {
        let eventsDetalizationVC = EventsDetalizationViewController.instantiate
        eventsDetalizationVC.eventId = eventId
        navigation?.present(eventsDetalizationVC, animated: animated, completion: nil)
        
        return eventsDetalizationVC
    }
    
    func navigateToPeople(animated: Bool = true) {
        let peopleVC = PeopleViewController.instantiate
        navigation?.pushViewController(peopleVC, animated: animated)
    }
    
    func navigateToProjects(animated: Bool = true) {
        let projectsVC = ProjectsViewController.instantiate
        navigation?.pushViewController(projectsVC, animated: animated)
    }
    
    func navigateToInfo(animated: Bool = true) {
        let infoVC = InfoViewController.instantiate
        navigation?.pushViewController(infoVC, animated: animated)
    }
    
    func navigateToWorkerDetalization(animated: Bool = true, workerId: String) -> WorkerDetalizationViewController {
        let workerDetalizationVC = WorkerDetalizationViewController.instantiate
        workerDetalizationVC.workerId = workerId
        navigation?.present(workerDetalizationVC, animated: animated, completion: nil)
        
        return workerDetalizationVC
    }
    
    func navigateToProjectDetalization(animated: Bool = true,
                                       projectId: String) -> ProjectDetalizationViewController {
        let projectDetalizationVC = ProjectDetalizationViewController.instantiate
        projectDetalizationVC.projectId = projectId
        navigation?.present(projectDetalizationVC, animated: animated, completion: nil)
        
        return projectDetalizationVC
    }
    
    func navigateToContainer(animated: Bool = true) {
        let containerVC = ContainerViewController.instantiate
        let homeVC = HomeViewController.instantiate
        containerVC.addViewController(viewController: homeVC)
        navigation?.pushViewController(containerVC, animated: animated)
    }
}
