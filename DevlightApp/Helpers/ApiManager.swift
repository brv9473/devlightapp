//
//  Manager.swift
//  DevlightApp
//
//  Created by Rostyslav Bodnar on 22.10.2020.
//

import Foundation
import Alamofire

final class ApiManager {
    static let stingUrl = URL(string: "\(Constans.Url.baseUrl)")
    
    class func sendGet<T: Decodable>(endPoint: String,
                                     parameters: Parameters? = nil,
                                     headers: HTTPHeaders?,
                                     completion: ((Swift.Result<T, Error>) -> Void)?) {
        
        AF.request(stingUrl?.appendingPathComponent(endPoint).absoluteString ?? "0",
                   method: .get,
                   parameters: parameters,
                   headers: headers)
            .response { response in
                
                do {
                    guard let data = response.data else {
                        print(">>> Response is empty")
                        completion?(.failure(NSError(domain: "Empty response", code: 0, userInfo: nil)))
                        return
                    }
                    
                    let someResult = try JSONDecoder().decode(T.self, from: data)
                    print(someResult)
                    print("__________")
                    completion?(.success(someResult))
                    print(">>>> Response: \(String(describing: String(data: data, encoding: .utf8)))")
                    
                } catch {
                    completion?(.failure(error))
                    print("Decodind error: \(error)")
                }
            }
    }
    
    class func sendPost<T: Encodable>(endPoint: String, parameters: T?) {
        let encoder = JSONParameterEncoder.default
        
        AF.request(stingUrl?.appendingPathComponent(endPoint).absoluteString ?? "0",
                   method: .post,
                   parameters: parameters,
                   encoder: encoder)
            .response { response in
                
                guard let data = response.data else {
                    print(">>> Response is empty")
                    return
                }
                
                print(">>>> Response: \(String(describing: String(data: data, encoding: .utf8)))")
            }
    }
    
    class func sendPostWithResponse<T: Encodable, D: Decodable>(endPoint: String,
                                                                headers: HTTPHeaders?,
                                                                parameters: T?,
                                                                completion: ((Result<D, Error>) -> Void)?) {
        let encoder = JSONParameterEncoder.default
        
        AF.request(stingUrl?.appendingPathComponent(endPoint).absoluteString ?? "0",
                   method: .post,
                   parameters: parameters,
                   encoder: encoder,
                   headers: headers)
            .response { response in
                
                do {
                    guard let data = response.data else {
                        print(">>> Response is empty")
                        completion?(.failure(NSError(domain: "Empty response", code: 0, userInfo: nil)))
                        return
                    }
                    
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    
                    if let error = try? decoder.decode(ErrorResponseModel.self, from: data) {
                        completion?(.failure(error))
                        
                    } else {
                        let someResult = try decoder.decode(D.self, from: data)
                        print(someResult)
                        print("__________")
                        completion?(.success(someResult))
                        print(">>>> Response: \(String(describing: String(data: data, encoding: .utf8)))")
                    }
                    
                } catch {
                    completion?(.failure(error))
                    print("Decodind error: \(error)")
                }
            }
    }
    
    class func sendGetWithResponse<T: Encodable, D: Decodable>(endPoint: String,
                                                               headers: HTTPHeaders?,
                                                               parameters: T?,
                                                               completion: ((Result<D, Error>) -> Void)?) {
        
        let encoder = URLEncodedFormParameterEncoder.default
        
        AF.request(stingUrl?.appendingPathComponent(endPoint).absoluteString ?? "0",
                   method: .get,
                   parameters: parameters,
                   encoder: encoder,
                   headers: headers)
            
            .response { response in
                
                do {
                    guard let data = response.data else {
                        print(">>> Response is empty")
                        completion?(.failure(NSError(domain: "Empty response", code: 0, userInfo: nil)))
                        return
                    }
                    
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    
                    if let error = try? decoder.decode(ErrorResponseModel.self, from: data) {
                        completion?(.failure(error))
                        
                        if error.code == 10009 {
                            Service.init().refreshToken(params: RefreshTokenModel(refreshToken:
                                                                                    "sfdf")) { result in
                                switch result {
                                case .success(let model):
                                    let defaults = UserDefaults.standard
                                    defaults.setValue(model.token, forKey: "token")
                                    defaults.setValue(model.refreshToken,
                                                      forKey: "refreshToken")
                                    
                                case .failure(let error):
                                    print(error)
                                }
                            }
                        }
                        
                    } else {
                        let someResult = try decoder.decode(D.self, from: data)
                        print(someResult)
                        print("__________")
                        completion?(.success(someResult))
                        print(">>>> Response: \(String(describing: String(data: data, encoding: .utf8)))")
                    }
                    
                } catch {
                    completion?(.failure(error))
                    print("Decodind error: \(error)")
                }
            }
    }
}
